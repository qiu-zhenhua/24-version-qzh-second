/**
 * @file RuneParam.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 神符参数管理
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "RuneParam.h"
#include <opencv2/core/persistence.hpp>

using namespace std;
using namespace cv;

/**
 * @brief 从 yml 中获取神符参数
 * 
 * @param path yml 文件路径
 */
RuneParam::RuneParam(const string &path)
{
    FileStorage fp(path, FileStorage::READ);
    fp["MIN_AREA_RATIO"] >> MIN_AREA_RATIO;
    fp["MIN_FEATURE_DISTANCE"] >> MIN_FEATURE_DISTANCE;
    fp["MAX_FEATURE_DISTANCE"] >> MAX_FEATURE_DISTANCE;
}
RuneParam rune_param("/usr/local/etc/RuneParam.yml");