/**
 * @file ArmorParam.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 装甲板参数管理
 * @version 1.0
 * @date 2021-09-11
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "ArmorParam.h"
#include <opencv2/core/persistence.hpp>

using namespace std;
using namespace cv;

/**
 * @brief 从yml文件中读取装甲板参数
 * 
 * @param param 文件路径
 */
ArmorParam::ArmorParam(const string &param)
{
    FileStorage arm(param, FileStorage::READ);
    arm["MAX_DELTA_ANGLE"] >> this->MAX_DELTA_ANGLE;
    arm["MAX_LENGTH_RATIO"] >> this->MAX_LENGTH_RATIO;
    arm["MAX_WIDTH_RATIO"] >> this->MAX_WIDTH_RATIO;
    arm["SCALE_THRESHOLD"] >> this->SCALE_THRESHOLD;
    arm["WIDTH_SCALE_THRESHOLD"] >> this->WIDTH_SCALE_THRESHOLD;
    arm["TILT_SCALE_THRESHOLD"] >> this->TILT_SCALE_THRESHOLD;
    arm["TILT_ANGLE_THRESHOLD"] >> this->TILT_ANGLE_THRESHOLD;
    arm["MAX_DISTANCE_LENGTH_RATIO"] >> this->MAX_DISTANCE_LENGTH_RATIO;
    arm["MIN_DISTANCE_LENGTH_RATIO"] >> this->MIN_DISTANCE_LENGTH_RATIO;
    arm["MAX_UP_DOWN_DIS_RATIO"] >> this->MAX_UP_DOWN_DIS_RATIO;
    arm["MIN_CROSS_RATIO"] >> this->MIN_CROSS_RATIO;
    arm["MIN_DISLOCATION_RATIO"] >> this->MIN_DISLOCATION_RATIO;

    arm["ERROR_LENGTH_SCALE_RATIO"] >> this->ERROR_LENGTH_SCALE_RATIO;
    arm["ERROR_WIDTH_SCALE_RATIO"] >> this->ERROR_WIDTH_SCALE_RATIO;
    arm["ERROR_ANGLE_SCALE_RATIO"] >> this->ERROR_ANGLE_SCALE_RATIO;
    arm["ERROR_DISLOCATION_RATIO"] >> this->ERROR_DISLOCATION_RATIO;
    arm["ERROR_ARMOR_SCALE_RATIO"] >> this->ERROR_ARMOR_SCALE_RATIO;

    arm["SMALL_COEFF_W"] >> this->SMALL_COEFF_W;
    arm["BIG_COEFF_W"] >> this->BIG_COEFF_W;

    arm["MAX_DELTA_DIS"] >> this->MAX_DELTA_DIS;
    arm["MAX_DELTA_DIS_RATIO"] >> this->MAX_DELTA_DIS_RATIO;
    arm["MAX_DIRECTION_RATIO"] >> this->MAX_DIRECTION_RATIO;
}
ArmorParam armor_param("/usr/local/etc/ArmorParam.yml");