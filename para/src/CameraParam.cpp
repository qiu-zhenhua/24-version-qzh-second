/**
 * @file CameraParam.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "CameraParam.h"
#include <opencv2/core/persistence.hpp>

using namespace std;
using namespace cv;

/**
 * @brief 从yml文件中读取相机参数
 * 
 * @param param 文件路径
 */
CameraParam::CameraParam(const string &param)
{
    FileStorage cam(param, FileStorage::READ);
    cam["camera_matrix"] >> this->cameraMatrix;
    cam["distortion_coefficients"] >> this->distCoeff;

    cam["cam_exposure"] >> this->cam_exposure;
    cam["cam_gamma"] >> this->cam_gamma;
    cam["cam_contrast"] >> this->cam_contrast;
    cam["cam_Bgain"] >> this->cam_Bgain;
    cam["cam_Ggain"] >> this->cam_Ggain;
    cam["cam_Rgain"] >> this->cam_Rgain;

    cam["image_width"] >> this->image_width;
    cam["image_height"] >> this->image_height;
}
CameraParam camera_param("/usr/local/etc/CameraParam.yml");