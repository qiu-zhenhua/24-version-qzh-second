/**
 * @file JudgeGyroParam.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 小陀螺参数管理
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "JudgeGyroParam.h"
#include <opencv2/core/persistence.hpp>

using namespace std;
using namespace cv;

/**
 * @brief 从yml文件中读取小陀螺参数
 * 
 * @param param 文件路径
 */
JudgeGyroParam::JudgeGyroParam(const string &param)
{
    FileStorage gyro(param, FileStorage::READ);
    gyro["REVERSAL_ARMOR_RATIO"] >> this->REVERSAL_ARMOR_RATIO;
    gyro["SPEED_ANGLE_THRESHOLD"] >> this->SPEED_ANGLE_THRESHOLD;
    gyro["MAX_TILT_CAPACITY"] >> this->MAX_TILT_CAPACITY;
    gyro["GYRO_THRESHOLD"] >> this->GYRO_THRESHOLD;
    gyro["GYRO_SPACING"] >> this->GYRO_SPACING;
    gyro["GYRO_DISTANCE"] >> this->GYRO_DISTANCE;
}
JudgeGyroParam judge_gyro_param("/usr/local/etc/JudgeGyroParam.yml");
