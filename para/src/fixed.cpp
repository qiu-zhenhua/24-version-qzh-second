/**
 * @file fixed.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 固定参数
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "fixed.h"

using namespace std;
using namespace cv;

const vector<Point3f> wSMALL_ARMOR =
    {{-65.5, 26.5, 7},
     {-65.5, -26.5, -7},
     {65.5, -26.5, -7},
     {65.5, 26.5, 7}};
const vector<Point3f> wBIG_ARMOR =
    {{-113, 26.5, 7},
     {-113, -26.5, -7},
     {113, -26.5, -7},
     {113, 26.5, 7}};
const vector<Point3f> wRUNE_CONTOUR =
    {{-113, 70, 0},
     {-113, -70, 0},
     {113, -70, 0},
     {113, 70, 0},
     {0, 700, 0}};
const char CAM_SLING[32] = "041062620082";