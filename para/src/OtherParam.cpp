/**
 * @file OtherParam.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 其余参数管理
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "OtherParam.h"
#include <opencv2/core/persistence.hpp>

using namespace std;
using namespace cv;

/**
 * @brief 从yml文件中读取其他参数
 * 
 * @param param 文件路径
 */
OtherParam::OtherParam(const string &param)
{
    FileStorage oth(param, FileStorage::READ);
    oth["GRAY_THRESHOLD_RED"] >> this->GRAY_THRESHOLD_RED;
    oth["GRAY_THRESHOLD_BLUE"] >> this->GRAY_THRESHOLD_BLUE;

    oth["INIT_PITCH"] >> this->INIT_PITCH;
    oth["INIT_YAW"] >> this->INIT_YAW;
    oth["INIT_COLOR_MODE"] >> this->INIT_COLOR_MODE;
    oth["INIT_SHOOT_SPEED"] >> this->INIT_SHOOT_SPEED;
    oth["INIT_MODE"] >> this->INIT_MODE;

    oth["point_scale"] >> this->point_scale;
}
OtherParam other_param("/usr/local/etc/OtherParam.yml");