/**
 * @file KalmanFilterParam.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 滤波器参数管理
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "KalmanFilterParam.h"
#include <opencv2/core/persistence.hpp>

using namespace std;
using namespace cv;

/**
 * @brief 从yml文件中读取滤波器参数
 * 
 * @param param 文件路径
 */
KalmanFilterParam::KalmanFilterParam(const string &param)
{
    FileStorage kfp(param, FileStorage::READ);
    kfp["GYRO_PROCESS_ERROR"] >> this->GYRO_PROCESS_ERROR;
    kfp["GYRO_MEASURE_ERROR"] >> this->GYRO_MEASURE_ERROR;
    kfp["PROCESS_ERROR"] >> this->PROCESS_ERROR;
    kfp["MEASURE_ERROR"] >> this->MEASURE_ERROR;
    kfp["DIS_PROCESS_ERROR"] >> this->DIS_PROCESS_ERROR;
    kfp["DIS_MEASURE_ERROR"] >> this->DIS_MEASURE_ERROR;
    kfp["ANGLE_PROCESS_ERROR"] >> this->ANGLE_PROCESS_ERROR;
    kfp["ANGLE_MEASURE_ERROR"] >> this->ANGLE_MEASURE_ERROR;

    kfp["GYRO_DELTA_TIME_YAW_RATIO"] >> this->GYRO_DELTA_TIME_YAW_RATIO;
    kfp["GYRO_DELTA_TIME_PITCH_RATIO"] >> this->GYRO_DELTA_TIME_PITCH_RATIO;
    kfp["GYRO_DELTA_TIME_YAW_BIAS"] >> this->GYRO_DELTA_TIME_YAW_BIAS;
    kfp["GYRO_DELTA_TIME_PITCH_BIAS"] >> this->GYRO_DELTA_TIME_PITCH_BIAS;

    kfp["DELTA_TIME_YAW_RATIO"] >> this->DELTA_TIME_YAW_RATIO;
    kfp["DELTA_TIME_PITCH_RATIO"] >> this->DELTA_TIME_PITCH_RATIO;
    kfp["DELTA_TIME_YAW_BIAS"] >> this->DELTA_TIME_YAW_BIAS;
    kfp["DELTA_TIME_PITCH_BIAS"] >> this->DELTA_TIME_PITCH_BIAS;

    kfp["RUNE_DELTA_TIME_RATIO"] >> this->RUNE_DELTA_TIME_RATIO;
    kfp["RUNE_DELTA_TIME_BIAS"] >> this->RUNE_DELTA_TIME_BIAS;
}
KalmanFilterParam kalman_filter_param("/usr/local/etc/KalmanFilterParam.yml");