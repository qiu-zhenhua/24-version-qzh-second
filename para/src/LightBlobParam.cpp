/**
 * @file LightBlobParam.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 灯条参数管理
 * @version 1.0
 * @date 2021-09-11
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "LightBlobParam.h"
#include <opencv2/core/persistence.hpp>

using namespace std;
using namespace cv;

/**
 * @brief 从yml文件中读取灯条参数
 * 
 * @param param 文件路径
 */
LightBlobParam::LightBlobParam(const string &param)
{
    FileStorage lbp(param, FileStorage::READ);
    lbp["MIN_AREA"] >> this->MIN_AREA;
    lbp["MIN_LENGTH"] >> this->MIN_LENGTH;
    lbp["CLOSE_LENGTH"] >> this->CLOSE_LENGTH;
    lbp["MIN_FAR_RATIO"] >> this->MIN_FAR_RATIO;
    lbp["MAX_FAR_RATIO"] >> this->MAX_FAR_RATIO;
    lbp["MAX_CLOSE_RATIO"] >> this->MAX_CLOSE_RATIO;
    lbp["MAX_RATIO"] >> this->MAX_RATIO;
    lbp["SMALL_CENTER_BLOB_BIAS"] >> this->SMALL_CENTER_BLOB_BIAS;
    lbp["BIG_CENTER_BLOB_BIAS"] >> this->BIG_CENTER_BLOB_BIAS;
    lbp["MAX_ANGLE"] >> this->MAX_ANGLE;
}
LightBlobParam light_blob_param("/usr/local/etc/LightBlobParam.yml");