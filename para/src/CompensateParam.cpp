/**
 * @file CompensateParam.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "CompensateParam.h"
#include <opencv2/core/persistence.hpp>

using namespace std;
using namespace cv;

/**
 * @brief 从yml文件中读取补偿参数
 * 
 * @param param 文件路径
 */
CompensateParam::CompensateParam(const string &param)
{
    FileStorage com(param, FileStorage::READ);
    com["YAW_COMPENSATE"] >> this->YAW_COMPENSATE;
    com["RUNE_PITCH_COMPENSATE"] >> this->RUNE_PITCH_COMPENSATE;
    com["PITCH_COMPENSATE"] >> this->PITCH_COMPENSATE;
}
CompensateParam compensate_param("/usr/local/etc/CompensateParam.yml");