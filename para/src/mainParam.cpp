/**
 * @file mainParam.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 小陀螺参数管理
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "mainParam.h"
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

/**
 * @brief 从yml文件中读取小陀螺参数
 * 
 * @param param 文件路径
 */
mainParam::mainParam(const string &param)
{
    FileStorage gyro(param, FileStorage::READ);
    gyro["COLOR"] >> this->COLOR;
    gyro["GYRO_ANGLE_YAW"] >> this->GYRO_ANGLE_YAW;
    gyro["GYRO_ANGLE_PITCH"] >> this->GYRO_ANGLE_PITCH;
    gyro["SHOOT_SPEED"] >> this->SHOOT_SPEED;
}
mainParam main_param("/usr/local/etc/mainParam.yml");
