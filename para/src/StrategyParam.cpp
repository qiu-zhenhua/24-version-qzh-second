/**
 * @file StrategyParam.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 决策参数管理
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "StrategyParam.h"
#include <opencv2/core/persistence.hpp>

using namespace std;
using namespace cv;

/**
 * @brief 从yml文件中读取决策参数
 * 
 * @param param 文件路径
 */
StrategyParam::StrategyParam(const string &param)
{
    FileStorage strategy(param, FileStorage::READ);
    strategy["SENTRY_TRACK_FRAMES"] >> this->SENTRY_TRACK_FRAMES;
    strategy["NORMAL_TRACK_FRAMES"] >> this->NORMAL_TRACK_FRAMES;
    strategy["RECORD_FRAMES"] >> this->RECORD_FRAMES;
    strategy["MAX_DIS"] >> this->MAX_DIS;
    strategy["MAX_DIS_DIFF"] >> this->MAX_DIS_DIFF;
    strategy["GYRO_RADIUS_RATIO"] >> this->GYRO_RADIUS_RATIO;
    strategy["NORMAL_RADIUS_RATIO"] >> this->NORMAL_RADIUS_RATIO;
    strategy["SHOOT_CONFIDENCE_LEVEL"] >> this->SHOOT_CONFIDENCE_LEVEL;
    strategy["SHOOT_SPEED_15_BIAS"] >> this->SHOOT_SPEED_15_BIAS;
    strategy["SHOOT_SPEED_18_BIAS"] >> this->SHOOT_SPEED_18_BIAS;
    strategy["SHOOT_SPEED_30_BIAS"] >> this->SHOOT_SPEED_30_BIAS;
}
StrategyParam strategy_param("/usr/local/etc/StrategyParam.yml");