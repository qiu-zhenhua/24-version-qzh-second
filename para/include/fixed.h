/**
 * @file fixed.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 固定参数
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include <string>
#include <vector>
#include <opencv2/core/types.hpp>

extern const std::string CMAKE_CONFIG_FILES;         // 编译安装路径
extern const std::vector<cv::Point3f> wSMALL_ARMOR;  // 小装甲板世界坐标
extern const std::vector<cv::Point3f> wBIG_ARMOR;    // 大装甲板世界坐标
extern const std::vector<cv::Point3f> wRUNE_CONTOUR; // 神符轮廓世界坐标
extern const char CAM_SLING[32];                     // 吊射相机序列号