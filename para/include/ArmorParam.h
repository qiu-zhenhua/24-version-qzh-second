/**
 * @file ArmorParam.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 装甲板参数管理头文件
 * @version 1.0
 * @date 2021-09-11
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include <string>

// 装甲板参数
struct ArmorParam
{
    float MAX_DELTA_ANGLE;           // 左右灯条最大角度误差
    float MAX_LENGTH_RATIO;          // 左右灯条长度最大误差
    float MAX_WIDTH_RATIO;           // 左右灯条宽度最大误差
    float SCALE_THRESHOLD;           // 大小装甲板阈值
    float WIDTH_SCALE_THRESHOLD;     // 左右灯条宽度阈值
    float TILT_SCALE_THRESHOLD;      // 倾斜时大小装甲板阈值
    float TILT_ANGLE_THRESHOLD;      // 判断为不再倾斜的装甲板时的角度阈值
    float MAX_DISTANCE_LENGTH_RATIO; // 灯条拟合成的装甲板长宽的最大比例
    float MIN_DISTANCE_LENGTH_RATIO; // 灯条拟合成的装甲板长宽的最小比例
    float MAX_UP_DOWN_DIS_RATIO;     // 左右灯条顶点的投影长度与灯条长度的最大比例
    float MIN_CROSS_RATIO;           // 装甲板对角线长度与两灯条最大高度的临界比值
    float MIN_DISLOCATION_RATIO;     // 判断左右灯条极度错位的最小比值

    float ERROR_LENGTH_SCALE_RATIO; // 装甲板左右灯条长度匹配误差系数
    float ERROR_WIDTH_SCALE_RATIO;  // 装甲板左右灯条宽度匹配误差系数
    float ERROR_ANGLE_SCALE_RATIO;  // 装甲板左右灯条角度匹配误差系数
    float ERROR_DISLOCATION_RATIO;  // 装甲板左右灯条错位系数
    float ERROR_ARMOR_SCALE_RATIO;  // 装甲板长宽匹配误差系数

    float SMALL_COEFF_W; // 小装甲板 pnp - dis : ω
    float BIG_COEFF_W;   // 大装甲板 pnp - dis : ω

    float MAX_DELTA_DIS;       // 识别为相同装甲板序列时，装甲板中心在两帧之间允许的最大距离
    float MAX_DELTA_DIS_RATIO; // 识别为相同装甲板序列时，装甲板中心在两帧之间允许的最大距离比值
    float MAX_DIRECTION_RATIO; // 识别为相同装甲板序列时，装甲板中心速度反向时允许的最大比值

    ArmorParam(const std::string &param);
};

extern ArmorParam armor_param;
