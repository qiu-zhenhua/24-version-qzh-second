/**
 * @file OtherParam.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 其余参数管理头文件
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include <string>

// 其他参数
struct OtherParam
{
    int GRAY_THRESHOLD_RED;   // pre_handle -- 蓝色阈值
    int GRAY_THRESHOLD_BLUE;  // pre_handle -- 红色阈值
    float INIT_PITCH;         // GyroScope_init - 陀螺仪 - pitch 角度
    float INIT_YAW;           // GyroScope_init - 陀螺仪 - yaw 角度
    uint8_t INIT_COLOR_MODE;  // GyroScope_init - 己方颜色
    uint8_t INIT_SHOOT_SPEED; // GyroScope_init - 枪口射速
    uint8_t INIT_MODE;        // GyroScope_init - 模式
    int point_scale;          // 调试显示的点大小

    OtherParam(const std::string &param);
};

extern OtherParam other_param;
