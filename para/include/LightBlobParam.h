/**
 * @file LightBlobParam.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 灯条参数管理头文件
 * @version 1.0
 * @date 2021-09-11
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include <string>

// 灯条参数
struct LightBlobParam
{
    int MIN_AREA;                 // 灯条最小面积
    int MIN_LENGTH;               // 灯条最小长度
    int CLOSE_LENGTH;             // 近处灯条临界长度
    float MIN_FAR_RATIO;          // 远处灯条长宽最小比例
    float MAX_FAR_RATIO;          // 远处灯条长宽最大比例
    float MAX_CLOSE_RATIO;        // 近处灯条长宽最大比例
    float MAX_RATIO;              // 灯条长宽最大比例
    float SMALL_CENTER_BLOB_BIAS; // 小装甲板中心点到灯条的距离与高度的比值
    float BIG_CENTER_BLOB_BIAS;   // 大装甲板中心点到灯条的距离与高度的比值
    int MAX_ANGLE;                // 与竖直方向的最大角度

    LightBlobParam(const std::string &param);
};

extern LightBlobParam light_blob_param;