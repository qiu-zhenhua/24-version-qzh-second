/**
 * @file CompensateParam.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 补偿参数管理头文件
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include <string>

// 补偿参数
struct CompensateParam
{
    float YAW_COMPENSATE;        // yaw 额定补偿
    float RUNE_PITCH_COMPENSATE; // 打符 pitch 额定补偿
    float PITCH_COMPENSATE;      // pitch 额定补偿

    CompensateParam(const std::string &param);
};

extern CompensateParam compensate_param;