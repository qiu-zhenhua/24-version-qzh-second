/**
 * @file StrategyParam.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 决策参数管理头文件
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include <string>

// 决策参数
struct StrategyParam
{
    int SENTRY_TRACK_FRAMES;      // Armor - 哨兵模式追踪帧数
    int NORMAL_TRACK_FRAMES;      // Armor - 普通模式
    int RECORD_FRAMES;            // Armor - 时间序列记录最大帧数
    int MAX_DIS;                  // Armor - 最远距离
    int MAX_DIS_DIFF;             // Armor - 最大切换装甲板距离差值
    float GYRO_RADIUS_RATIO;      // 击打小陀螺时射击半径比值
    float NORMAL_RADIUS_RATIO;    // 普通模式时射击半径比值
    float SHOOT_CONFIDENCE_LEVEL; // 普通模式下预测点的置信度
    int SHOOT_SPEED_15_BIAS;      // 15m/s 弹速负偏置
    int SHOOT_SPEED_18_BIAS;      // 18m/s 弹速负偏置
    int SHOOT_SPEED_30_BIAS;      // 30m/s 弹速负偏置

    StrategyParam(const std::string &param);
};

// 决策参数
extern StrategyParam strategy_param;
