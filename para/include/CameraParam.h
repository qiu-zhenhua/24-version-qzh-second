/**
 * @file CameraParam.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 相机参数管理头文件
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include <opencv2/core/mat.hpp>
#include <string>

// 相机参数
struct CameraParam
{
    cv::Mat cameraMatrix; // 相机内参
    cv::Mat distCoeff;    // 畸变参数
    int image_width;      // 相机宽度和高度
    int image_height;     // 相机宽度和高度
    int cam_exposure;     // 相机曝光
    int cam_gamma;        // 相机 Gamma 值
    int cam_contrast;     // 相机对比度
    int cam_Bgain;        // 相机蓝色增益
    int cam_Ggain;        // 相机绿色增益
    int cam_Rgain;        // 相机红色增益

    CameraParam(const std::string &param);
};

extern CameraParam camera_param;