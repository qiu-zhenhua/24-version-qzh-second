/**
 * @file RuneParam.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 神符参数管理头文件
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include <string>

// 神符参数
struct RuneParam
{
    float MIN_AREA_RATIO;       // 最小面积比值
    float MIN_FEATURE_DISTANCE; // 特征间距与神符装甲板高度的最小比值
    float MAX_FEATURE_DISTANCE; // 特征间距与神符装甲板高度的最大比值

    RuneParam(const std::string &path);
};

extern RuneParam rune_param;
