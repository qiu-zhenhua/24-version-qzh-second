/**
 * @file JudgeGyroParam.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 小陀螺参数管理头文件
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include <string>

// 小陀螺参数
struct JudgeGyroParam
{
    float REVERSAL_ARMOR_RATIO;  // 基础反转量系数
    float SPEED_ANGLE_THRESHOLD; // 判断更新为倾斜装甲板的阈值量 (speed × angle)
    int MAX_TILT_CAPACITY;       // 倾斜装甲板标志位容器最大容量
    int GYRO_THRESHOLD;          // 发弹策略转变为小陀螺模式的标志位容器阈值
    float GYRO_SPACING;          // 小陀螺装甲板间距与装甲板平均高度的比例系数
    float GYRO_DISTANCE;         // 小陀螺装甲板距离差异

    JudgeGyroParam(const std::string &param);
};

extern JudgeGyroParam judge_gyro_param;