/**
 * @file KalmanFilterParam.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 滤波参数管理头文件
 * @version 1.0
 * @date 2021-09-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include <string>

// 滤波器参数
struct KalmanFilterParam
{
    double GYRO_PROCESS_ERROR;  // Armor - 普通模式过程噪声协方差系数
    double GYRO_MEASURE_ERROR;  // Armor - 普通模式测量噪声协方差系数
    double PROCESS_ERROR;       // Armor - 哨兵模式过程噪声协方差系数
    double MEASURE_ERROR;       // Armor - 哨兵模式测量噪声协方差系数
    double DIS_PROCESS_ERROR;   // Armor - 距离过程噪声协方差系数
    double DIS_MEASURE_ERROR;   // Armor - 距离测量噪声协方差系数
    double ANGLE_PROCESS_ERROR; // Rune - 角度过程噪声协方差系数
    double ANGLE_MEASURE_ERROR; // Rune - 角度测量噪声协方差系数

    float GYRO_DELTA_TIME_YAW_RATIO;   // Armor - 普通模式 yaw 方向飞行时间补偿系数
    float GYRO_DELTA_TIME_PITCH_RATIO; // Armor - 普通模式 pitch 方向飞行时间补偿系数
    float GYRO_DELTA_TIME_YAW_BIAS;    // Armor - 普通模式 yaw 方向飞行时间补偿偏置
    float GYRO_DELTA_TIME_PITCH_BIAS;  // Armor - 普通模式 pitch 方向飞行时间补偿偏置

    float DELTA_TIME_YAW_RATIO;   // Armor - 哨兵模式 yaw 方向飞行时间补偿系数
    float DELTA_TIME_PITCH_RATIO; // Armor - 哨兵模式 pitch 方向飞行时间补偿系数
    float DELTA_TIME_YAW_BIAS;    // Armor - 哨兵模式 yaw 方向飞行时间补偿偏置
    float DELTA_TIME_PITCH_BIAS;  // Armor - 哨兵模式 pitch 方向飞行时间补偿偏置

    float RUNE_DELTA_TIME_RATIO; // Rune - 神符模式飞行时间补偿系数
    float RUNE_DELTA_TIME_BIAS;  // Rune - 神符模式飞行时间补偿偏置

    KalmanFilterParam(const std::string &param);
};

// 滤波器参数
extern KalmanFilterParam kalman_filter_param;