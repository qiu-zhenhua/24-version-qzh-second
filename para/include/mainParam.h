
#include <string>
#include <opencv2/opencv.hpp>

// 小陀螺参数
struct mainParam
{
    bool COLOR;
    float GYRO_ANGLE_YAW;
    float GYRO_ANGLE_PITCH;
    uint8_t SHOOT_SPEED;
    mainParam(const std::string &param);
};

extern mainParam main_param;