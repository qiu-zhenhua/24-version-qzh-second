/**
 * @file File.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 日志管理
 * @version 1
 * @date 2021-07-11
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include <iostream>
#include <ctime>
#include "Log.h"

using namespace std;

/**
 * @brief Construct a new File:: File object
 * 
 * @param file_name 
 */
Log::Log(const string &file_name)
{
    this->init = false;
    this->last_second = -1;
    this->setFileName(file_name);
    // is_open
    if (this->ofs.is_open() && this->ifs.is_open())
    {
        this->init = true;
    }
}

/**
 * @brief Destroy the File:: File object
 */
Log::~Log()
{
    this->ofs.close();
    this->ifs.close();
}

/**
 * @brief write message to a file
 * 
 * @param str the content that you want to write 
 */
void Log::write(const string &str)
{
    // get time
    string time = this->getTime();
    // 
    if (this->present_second == this->last_second)
    {
        return;
    }
    else
    {
        if (this->init)
        {
            ofs << "[ " << time << " ] " << str << endl;
        }
        // update
        this->last_second = this->present_second;
    }
}

/**
 * @brief get time and date
 * 
 * @return string - time and date 
 */
string Log::getTime()
{
    // get present time
    time_t *now = new time_t(time(0));
    tm *ltm = localtime(now);
    // get date
    int year = 1900 + ltm->tm_year;
    string year_str = to_string(year);

    int month = 1 + ltm->tm_mon;
    string month_str = (month < 10) ? "0" + to_string(month) : to_string(month);

    int day = ltm->tm_mday;
    string day_str = (day < 10) ? "0" + to_string(day) : to_string(day);

    // get time
    int hour = ltm->tm_hour;
    string hour_str = (hour < 10) ? "0" + to_string(hour) : to_string(hour);

    int minute = ltm->tm_min;
    string minute_str = (minute < 10) ? "0" + to_string(minute) : to_string(minute);

    int second = ltm->tm_sec;
    string second_str = (second < 10) ? "0" + to_string(second) : to_string(second);
    this->present_second = second;

    string date_time = year_str + "-" + month_str + "-" + day_str + " " +
                       hour_str + ":" + minute_str + ":" + second_str;

    // set null
    if (now != nullptr)
    {
        delete now;
        now = nullptr;
    }
    return date_time;
}

/**
 * @brief set file name
 * 
 * @param file_name
 */
void Log::setFileName(const string &file_name)
{
    // ifstream
    if (this->ifs.is_open())
    {
        this->ifs.close();
    }
    this->ifs.open(file_name);
    // ofstream
    if (this->ofs.is_open())
    {
        this->ofs.close();
    }
    this->ofs.open(file_name, ios::app);
}