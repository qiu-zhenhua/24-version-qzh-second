/**
 * @file File.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 日志管理头文件
 * @version 1.0
 * @date 2021-07-11
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include <fstream>
#include <memory>

class Log
{
public:
private:
    bool init;
    std::ofstream ofs;
    std::ifstream ifs;
    std::string file_name;

    uint8_t last_second;
    uint8_t present_second;

public:
    Log() {}
    Log(const std::string &);
    ~Log();
    void setFileName(const std::string &);
    void write(const std::string &);

private:
    std::string getTime();
};
