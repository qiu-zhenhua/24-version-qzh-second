/**
 * @file Math.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 额外数据函数库
 * @version 1.0
 * @date 2021-06-14
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include <opencv2/core/mat.hpp>

// --------------------【结构、类型、常量定义】--------------------

extern const float PI; // 圆周率
extern const float e;  // 自然对数底数
extern const float g;  // 重力加速度

// pnp 姿态解算
struct ResultPnP
{
    double yaw;
    double pitch;
    double roll;
    cv::Mat tran_vec;
    cv::Mat rotat_vec;
    float distance;
};

// 定义 Matx11
namespace cv
{
	using Matx11f = Matx<float, 1, 1>;
	using Matx11d = Matx<double, 1, 1>;
}

// ------------------------【广义位移计算】------------------------
// 获取距离
inline float getDistances(cv::Point2f pt_1, cv::Point2f pt_2)
{
    return sqrt(pow(pt_1.x - pt_2.x, 2) + pow(pt_1.y - pt_2.y, 2));
}
// 获取与水平方向的夹角
inline float getHorizontalAngle(const cv::Point2f &start, const cv::Point2f &end)
{
    return -atanf((end.y - start.y) / (end.x - start.x));
}
// 获取角度差
float getDeltaAngle(float, float);
// 计算相机中心相对于装甲板中心的角度
cv::Point2f calculateRelativeAngle(const cv::Mat &, const cv::Mat &, cv::Point2f);
// 计算装甲板中心的像素坐标
cv::Point2f calculateRelativeCenter(const cv::Mat &, const cv::Mat &, cv::Point2f);

// ------------------------【常用数学公式】------------------------
// y = sec(x)
inline float sec(float x) { return 1 / cosf(x); }
// y = csc(x)
inline float csc(float x) { return 1 / sinf(x); }
// y = cot(x)
inline float cot(float x) { return 1 / tanf(x); }
// ° -> rad
inline float deg2rad(float deg) { return deg * PI / 180.f; }
// rad -> °
inline float rad2deg(float rad) { return rad * 180.f / PI; }

// ------------------------【常用变换公式】------------------------
/**
 * @brief Matx 类型向量转化为 vector 类型向量
 * 
 * @tparam _Mat Matx 类型
 * @tparam _Vec vector 类型
 * @param mat 修改前的向量
 * @param vec 修改后的向量
 */
template <typename _Mat, typename _Vec>
void matx2vec(_Mat &mat, _Vec &vec)
{
    int num = mat.cols * mat.rows;
    vec.clear();
    vec.reserve(num);
    for (int i = 0; i < num; i++)
    {
        vec.push_back(mat(i));
    }
}
