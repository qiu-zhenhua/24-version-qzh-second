/**
 * @file KalmanFilterX.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 轻量版卡尔曼滤波器
 * @version 1.0
 * @date 2021-08-12
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "KalmanFilterX.hpp"
