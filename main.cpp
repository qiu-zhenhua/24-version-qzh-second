#ifdef ARMOR
#include "ArmorProcessor.h"
#endif

#ifdef RUNE
#include "RuneProcessor.h"
#endif

#include "video.h"
#include "mainParam.h"
using namespace std;
using namespace cv;
int main()
{
    const Point2f gyro_angle={main_param.GYRO_ANGLE_YAW,main_param.GYRO_ANGLE_PITCH};
    #ifdef ARMOR
    processor_ptr processor = make_shared<ArmorProcessor>(main_param.COLOR,gyro_angle,main_param.SHOOT_SPEED);
    supporter_ptr a = make_shared<video>("../avi/1.avi",processor);
    #endif

    #ifdef RUNE
    processor_ptr processor = make_shared<RuneProcessor>(main_param.COLOR,gyro_angle,main_param.SHOOT_SPEED);
    supporter_ptr a = make_shared<video>("../avi/2.avi",processor);
    #endif
    a->start();
    return 0;
};
