/**
 * @file SingleArmorPredictor.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 单一时间序列的装甲板预测派生类
 * @version 1.0
 * @date 2021-08-26
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include <opencv2/core/utility.hpp>
#include "SingleArmorPredictor.h"
#include "KalmanFilterParam.h"
#include "CameraParam.h"

using namespace cv;
using namespace std;

/**
 * @brief 通过单一装甲板时间序列生成 4 阶卡尔曼滤波器，来创建对应的装甲板预测器
 * 
 * @param tracker 时间序列追踪器共享指针
 * @param mode 预测方案
 */
SingleArmorPredictor::SingleArmorPredictor(tracker_ptr &tracker, uint8_t mode) : single_predictor(tracker)
{
    // 根据预测方案选择滤波器
    switch (mode)
    {
    case 1U:
        this->__filter = KalmanFilter44(kalman_filter_param.PROCESS_ERROR,
                                        kalman_filter_param.MEASURE_ERROR);
        break;
    default:
        this->__filter = KalmanFilter44(kalman_filter_param.GYRO_PROCESS_ERROR,
                                        kalman_filter_param.GYRO_MEASURE_ERROR);
        break;
    }
    this->__filter.setH(Matx44f::eye());
}

/**
 * @brief 更新预测器
 * 
 * @param flying_time 子弹飞行时间
 * @param mode 预测方案
 */
void SingleArmorPredictor::updatePredictor(const float &flying_time, uint8_t mode)
{
    // 卡尔曼滤波器是否初始化
    if (this->__is_predict_init)
    {
        // 根据预测方案选择子弹飞行时间修正值
        float predict_yaw_time, predict_pitch_time;
        switch (mode)
        {
        case 1U: // 哨兵击打预测方案
            predict_yaw_time = flying_time * kalman_filter_param.DELTA_TIME_YAW_RATIO +
                               kalman_filter_param.DELTA_TIME_YAW_BIAS;
            predict_pitch_time = flying_time * kalman_filter_param.DELTA_TIME_PITCH_RATIO +
                                 kalman_filter_param.DELTA_TIME_PITCH_BIAS;
            break;
        default:
            predict_yaw_time = flying_time * kalman_filter_param.GYRO_DELTA_TIME_YAW_RATIO +
                               kalman_filter_param.GYRO_DELTA_TIME_YAW_BIAS;
            predict_pitch_time = flying_time * kalman_filter_param.GYRO_DELTA_TIME_PITCH_RATIO +
                                 kalman_filter_param.GYRO_DELTA_TIME_PITCH_BIAS;
            break;
        }

        // 状态转移矩阵
        this->__filter.setA(Matx44f{
            1, 0, predict_yaw_time, 0,
            0, 1, 0, predict_pitch_time,
            0, 0, 1, 0,
            0, 0, 0, 1});

        // 不出现闪烁
        if (!this->__pTracker->getVanishNumber() > 0)
        {
            // init
            float total_time = 0.1f;
            Point2f gyro_speed, relative_speed;
            // 队列长度足够长，则在获取时间、速度时采用逐差法
            if (this->__pTracker->getSize() >= 4U)
            {
                // 帧差时间
                total_time = (__pTracker->at(0)->getTime() +
                              __pTracker->at(1)->getTime() -
                              __pTracker->at(2)->getTime() -
                              __pTracker->at(3)->getTime()) /
                             getTickFrequency() / 2.f;
                // 陀螺仪角速度
                gyro_speed = (__pTracker->at(0)->getGyroAngle() +
                              __pTracker->at(1)->getGyroAngle() -
                              __pTracker->at(2)->getGyroAngle() -
                              __pTracker->at(3)->getGyroAngle()) /
                             (2.f * total_time);
                // 相对角速度
                relative_speed = (__pTracker->at(0)->getRelativeAngle() +
                                  __pTracker->at(1)->getRelativeAngle() -
                                  __pTracker->at(2)->getRelativeAngle() -
                                  __pTracker->at(3)->getRelativeAngle()) /
                                 (2.f * total_time);
            }
            // 队列长度不够长，则在获取时间、速度时选择首尾取平均值
            else if (this->__pTracker->getSize() > 1 &&
                     this->__pTracker->getSize() < 4)
            {
                // 获取修正帧差时间
                total_time = (__pTracker->getFront()->getTime() -
                              __pTracker->getBack()->getTime()) /
                             getTickFrequency();
                // 陀螺仪角速度
                gyro_speed = (__pTracker->getFront()->getGyroAngle() -
                              __pTracker->getBack()->getGyroAngle()) /
                             total_time;
                // 相对角速度
                relative_speed = (__pTracker->getFront()->getRelativeAngle() -
                                  __pTracker->getBack()->getRelativeAngle()) /
                                 total_time;
            }
            // 绝对速度
            __speed = {relative_speed.x - gyro_speed.x,
                       relative_speed.y - gyro_speed.y,
                       0};
            // 更正测量值
            __filter.correct({__pTracker->getFront()->getRelativeAngle().x, // 位置_x
                              __pTracker->getFront()->getRelativeAngle().y, // 位置_y
                              __speed.x,                                    // 速度_x
                              __speed.y});                                  // 速度_y
        }
        // 预测
        Matx41f predict_mat = __filter.predict();
        // 更新卡尔曼预测值
        Matx31f predict_vec = {predict_mat(0),
                               predict_mat(1),
                               0.f};
        Matx31f speed_vec = {__speed.x,
                             __speed.y,
                             __speed.z};
        this->updatePredictMessage(predict_vec, speed_vec);
    }
    else
    {
        // 初始化为装甲板实际中心
        Matx41f init_mat(__pTracker->getFront()->getRelativeAngle().x,
                         __pTracker->getFront()->getRelativeAngle().y,
                         0.f, 0.f);
        // 初始化卡尔曼滤波器
        this->__filter.init(init_mat, 1e-5);
        // 更新卡尔曼预测值
        Matx31f init_vec = {init_mat(0), init_mat(1), 0.f};
        Matx31f speed_vec = {0.f, 0.f, 0.f};
        this->updatePredictMessage(init_vec, speed_vec);
        // 更新标志位
        this->__is_predict_init = true;
    }
}

/**
 * @brief 更新预测信息
 * 
 * @param predict_location 位置预测向量
 * @param speed_vec 速度向量
 */
void SingleArmorPredictor::updatePredictMessage(Matx31f &predict_vec, Matx31f &speed_vec)
{
    // 更新预测角度
    __pTracker->setPredictedAngle({predict_vec(0),
                                   predict_vec(1),
                                   predict_vec(2)});
    // 更新速度
    __pTracker->setSpeed({speed_vec(0),
                          speed_vec(1),
                          speed_vec(2)});
    // 更新预测中心点
    __pTracker->setPredictedCenter(calculateRelativeCenter(camera_param.cameraMatrix,
                                                           camera_param.distCoeff,
                                                           Point2f(predict_vec(0),
                                                                   predict_vec(1))));
}
