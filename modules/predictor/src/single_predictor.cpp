/**
 * @file single_predictor.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 抽象预测类
 * @version 1.0
 * @date 2021-08-24
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "single_predictor.h"

using namespace std;
using namespace cv;

/**
 * @brief 初始化预测器
 * 
 * @param tracker 时间序列追踪器共享指针
 */
single_predictor::single_predictor(tracker_ptr &tracker) : __pTracker(tracker)
{
    this->__is_predict_init = false;
    this->__speed = {0, 0, 0};
}