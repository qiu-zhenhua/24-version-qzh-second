/**
 * @file RunePredictor.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 神符预测派生类
 * @version 1.0
 * @date 2021-09-25
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "RunePredictor.h"

/**
 * @brief 神符预测核心函数
 * 
 * @param trackers 所有追踪器时间序列
 * @param mode 预测方案
 */
void RunePredictor::predict(std::vector<tracker_ptr> &trackers, uint8_t mode)
{
    // 更新预测器的匹配信息
    this->updateMatchMessage(trackers, mode);
    // 预测
    for (size_t i = 0; i < this->__predictors.size(); i++)
    {
        __predictors[i]->updatePredictor(trackers[i]->getTime(), mode);
    }
}

/**
 * @brief 更新时间序列与预测器的匹配信息
 * 
 * @param trackers 所有追踪器时间序列
 * @param mode 预测方案
 */
void RunePredictor::updateMatchMessage(std::vector<tracker_ptr> &trackers, uint8_t mode)
{
    // init
    if (this->__predictors.empty())
    {
        for (auto tracker : trackers)
        {
            __predictors.emplace_back(new SingleRunePredictor(tracker, mode));
            tracker->setPredictMessage(true);
        }
        return;
    }
    // 删除多余预测器
    __predictors.erase(remove_if(__predictors.begin(), __predictors.end(),
                                 [&](spredict_ptr &it)
                                 {
                                     return it->isTrackerNull();
                                 }),
                       __predictors.end());
    // 绑定新预测器
    for (auto tracker : trackers)
    {
        if (!tracker->getPredictMessage())
        {
            __predictors.emplace_back(new SingleRunePredictor(tracker, mode));
            tracker->setPredictMessage(true);
        }
    }
}