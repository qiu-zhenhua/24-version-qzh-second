/**
 * @file SingleRunePredictor.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 单一时间序列的神符预测派生类
 * @version 1.0
 * @date 2021-09-24
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "SingleRunePredictor.h"
#include "KalmanFilterParam.h"
#include "CameraParam.h"

using namespace std;
using namespace cv;

/**
 * @brief 通过单一神符时间序列生成 2 阶卡尔曼滤波器，来创建对应的神符预测器
 * 
 * @param tracker 时间序列追踪器共享指针
 * @param mode 预测方案
 */
SingleRunePredictor::SingleRunePredictor(tracker_ptr &tracker, uint8_t mode) : single_predictor(tracker)
{
    __filter = KalmanFilter22(kalman_filter_param.ANGLE_PROCESS_ERROR,
                              kalman_filter_param.ANGLE_MEASURE_ERROR);
    __filter.setH(Matx22f::eye());
}

/**
 * @brief 更新预测器
 * 
 * @param flying_time 子弹飞行时间
 * @param mode 预测方案
 */
void SingleRunePredictor::updatePredictor(const float &flying_time, uint8_t mode)
{
    if (__is_predict_init && __pTracker->getSize() >= 4U)
    {
        // ([0] - [2] + [1] - [3]) / 2 获取间隔一帧的两帧时间间隔
        float total_time = (__pTracker->at(0)->getTime() +
                            __pTracker->at(1)->getTime() -
                            __pTracker->at(2)->getTime() -
                            __pTracker->at(3)->getTime()) /
                           (2.f * getTickFrequency());
        // 设置状态转移矩阵
        float predict_time = kalman_filter_param.RUNE_DELTA_TIME_RATIO * flying_time +
                             kalman_filter_param.RUNE_DELTA_TIME_BIAS;
        __filter.setA(Matx<float, 2, 2>{1, predict_time, 0, 1});
        // 判断是否闪烁
        if (!this->__pTracker->getVanishNumber() > 0)
        {
            // 更新角速度
            float angular_velocity = (__pTracker->at(0)->getAngle() +
                                      __pTracker->at(1)->getAngle() -
                                      __pTracker->at(2)->getAngle() -
                                      __pTracker->at(3)->getAngle()) /
                                     (2.f * total_time);
            __speed = {angular_velocity, 0.f, 0.f};
            // 更新
            __filter.correct({__pTracker->getFront()->getAngle(),
                              __speed.x});
        }
        // 预测
        Matx21f predictVec = __filter.predict();

        // 预测向量
        Matx31f predict_mat = {predictVec(0),
                               0.f,
                               0.f};
        // 速度向量
        Matx31f speed_vec = {__speed.x,
                             __speed.y,
                             __speed.z};
        this->updatePredictMessage(predict_mat,
                                   speed_vec);
    }
    else
    {
        Matx21f init_mat = Matx21f(this->__pTracker->getFront()->getAngle(), 0.f);
        // 初始化卡尔曼滤波器
        __filter.init(init_mat, 1e-2);
        Matx31f init_vec = {init_mat(0), 0.f, 0.f};
        Matx31f speed_vec = {0.f, 0.f, 0.f};
        this->updatePredictMessage(init_vec, speed_vec);
        __is_predict_init = true;
    }
}

/**
 * @brief 更新预测信息
 * 
 * @param predict_location 位置预测向量
 * @param speed_vec 速度向量
 */
void SingleRunePredictor::updatePredictMessage(Matx31f &predict_vec, Matx31f &speed_vec)
{
    // 图像中的神符角度
    float image_angle = deg2rad(predict_vec(0));
    // 图像中的预测点
    float center_x = __pTracker->getFront()->getSecondary()->getCenter().x + __pTracker->getFront()->getFeatureDis() * cosf(image_angle);
    float center_y = __pTracker->getFront()->getSecondary()->getCenter().y - __pTracker->getFront()->getFeatureDis() * sinf(image_angle);
    __pTracker->setPredictedCenter({center_x, center_y});

    // 云台预测角度
    auto predict_angle = calculateRelativeAngle(camera_param.cameraMatrix,
                                                camera_param.distCoeff,
                                                __pTracker->getPredictedCenter());
    __pTracker->setPredictedAngle({predict_angle.x,
                                   predict_angle.y,
                                   0.f});
    // 更新速度
    __pTracker->setSpeed({speed_vec(0),
                          speed_vec(1),
                          speed_vec(2)});
}
