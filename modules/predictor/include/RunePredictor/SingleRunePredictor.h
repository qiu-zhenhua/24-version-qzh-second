/**
 * @file SingleRunePredictor.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 单一时间序列的神符预测派生类头文件
 * @version 1.0
 * @date 2021-09-24
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include "single_predictor.h"
#include "RuneTracker.h"
#include "KalmanFilterX.hpp"

// 单一时间序列的神符预测派生类
class SingleRunePredictor : public single_predictor
{
private:
    KalmanFilter22 __filter; // 卡尔曼滤波器

public:
    SingleRunePredictor(tracker_ptr &, uint8_t);           // 构造函数
    void updatePredictor(const float &, uint8_t) override; // 更新预测器

protected:
    void updatePredictMessage(cv::Matx31f &, cv::Matx31f &) override; // 更新预测信息
};
