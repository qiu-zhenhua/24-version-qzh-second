/**
 * @file RunePredictor.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 神符预测派生类头文件
 * @version 1.0
 * @date 2021-09-25
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include "predictor.h"
#include "SingleRunePredictor.h"

// 神符预测类
class RunePredictor : public predictor
{
private:
public:
    void predict(std::vector<tracker_ptr> &, uint8_t) override; // 神符预测核心函数

protected:
    void updateMatchMessage(std::vector<tracker_ptr> &, uint8_t) override; // 更新时间序列与预测器的匹配信息
};
