/**
 * @file single_predictor.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 单一时间序列的抽象预测类头文件
 * @version 1.0
 * @date 2021-08-26
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include "tracker.h"

// 单一时间序列的抽象预测类
class single_predictor
{
public:
protected:
    tracker_ptr __pTracker; // 时间序列追踪类指针
    bool __is_predict_init; // 预测信息是否初始化
    cv::Point3f __speed;    // 滤波处理后目标的速度: (x, y, z) 三个方向
    /**
     * @brief 具体单一时间序列对应的预测器暂时交给子类来定义
     */

public:
    /**
     * @brief 初始化预测器
     * 
     * @param tracker 时间序列追踪器共享指针
     * @param mode 预测方案
     */
    single_predictor(tracker_ptr &tracker);
    /**
     * @brief 更新预测器
     * 
     * @param time 预测量 (时间信息)
     * @param mode 预测方案
     */
    virtual void updatePredictor(const float &time, uint8_t mode) = 0;

    // 判断 tracker 是否为空
    inline bool isTrackerNull() { return (__pTracker == nullptr) ? true : (__pTracker->isVanish()); }

protected:
    virtual void updatePredictMessage(cv::Matx31f &, cv::Matx31f &) = 0; // 更新预测信息
};

// 单一时间序列的抽象预测类共享指针
using spredict_ptr = std::shared_ptr<single_predictor>;
