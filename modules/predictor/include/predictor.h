/**
 * @file predictor.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 抽象预测类头文件
 * @version 1.0
 * @date 2021-08-24
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include "single_predictor.h"

// 抽象预测类
class predictor
{
protected:
    std::vector<spredict_ptr> __predictors; // 所有时间序列 + 预测器

public:
    /**
     * @brief 预测核心函数
     * 
     * @param trackers 所有追踪器时间序列
     * @param mode 预测方案
     */
    virtual void predict(std::vector<tracker_ptr> &trackers, uint8_t mode = 0U) = 0;

protected:
    /**
     * @brief 更新时间序列与预测器的匹配信息
     * 
     * @param trackers 所有追踪器时间序列
     * @param mode 预测方案
     */
    virtual void updateMatchMessage(std::vector<tracker_ptr> &trackers, uint8_t mode = 0U) = 0;
};

// 抽象预测类共享指针
using predict_ptr = std::shared_ptr<predictor>;
