/**
 * @file SingleArmorPredictor.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 单一时间序列的装甲板预测派生类头文件
 * @version 1.0
 * @date 2021-08-26
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include "single_predictor.h"
#include "ArmorTracker.h"
#include "KalmanFilterX.hpp"

// 单一时间序列的装甲板预测类
class SingleArmorPredictor : public single_predictor
{
private:
    KalmanFilter44 __filter; // 卡尔曼滤波器

public:
    SingleArmorPredictor(tracker_ptr &, uint8_t);          // 构造函数
    void updatePredictor(const float &, uint8_t) override; // 更新预测器

protected:
    void updatePredictMessage(cv::Matx31f &, cv::Matx31f &) override; // 更新预测信息
};
