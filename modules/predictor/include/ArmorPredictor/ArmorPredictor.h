/**
 * @file ArmorPredictor.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 装甲板预测派生类头文件
 * @version 1.0
 * @date 2021-08-26
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include "predictor.h"
#include "SingleArmorPredictor.h"

// 装甲板预测类
class ArmorPredictor : public predictor
{
private:
public:
    void predict(std::vector<tracker_ptr> &, uint8_t) override; // 装甲板预测核心函数

protected:
    void updateMatchMessage(std::vector<tracker_ptr> &, uint8_t) override; // 更新时间序列与预测器的匹配信息
};
