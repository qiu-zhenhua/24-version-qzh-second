#include "video.h"

using namespace std;
using namespace cv;

video::video(string path,processor_ptr processor):supporter(path,processor)
{
    setPath(path);
    setProcessor(processor);
    __wait_time=20;
}

void video::start()
{
    VideoCapture cap(__path);
    // 设置视频帧位置
    this->__currentFrame=0;
    this->run=true;
    this->step=false;
    while(true)
    {   
        //--------------------------------------------------------------读取
        if(!cap.isOpened())
        {
            cout<<"empty"<<endl;
            continue;
        }
        cap.set(cv::CAP_PROP_POS_FRAMES, __currentFrame); 
        if (!cap.read(__init_img)) 
        {
            // 读取到视频末尾，如果要循环播放，可以重新设置cap的位置
            cap.set(cv::CAP_PROP_POS_FRAMES, 0);
            __currentFrame=0;
            continue;
        }
        //---------------------------------------------------------------识别
        if(run||step)
        {
            __processor->loadimg(__init_img);
            __processor->process();
            vector<tracker_ptr> a=__processor->get_trackers();
            for(int i =0;i<a.size();i++)
            {
                    circle(__init_img,a[i]->getPredictedCenter(),4, Scalar(0, 0, 255), -1);
                    circle(__init_img,a[i]->getFront()->getPrimary()->getCenter(),4, Scalar(0, 255, 255), -1);
                    circle(__init_img,a[i]->getFront()->getSecondary()->getCenter(),4, Scalar(255, 0, 0), -1);
            }
             imshow("b",__init_img);
            step=false;
        }

        //imshow("a",__init_img);

        //----------------------------------------------------------------控制
        int key;
        if(run)
        {
            key = cv::waitKey(__wait_time);
        }
        else
        {
            key = cv::waitKey(0);
        }
        switch(key)
        {
            //esc退出
            case 27:
            {
                return ;
            }
            //w控制速度
            case 119:
            {
                if(__wait_time>1)
                __wait_time--;
                else{cout<<"already max speed\n";};
                cout<<"speed: "<<__wait_time<<endl;
                run=false;
                break;
            }
            //s控制速度
            case 115:
            {
                __wait_time++;
                cout<<"speed: "<<__wait_time<<endl;
                run=false;
                break;
            }
            //空格控制暂停
            case 32:
            {
                run=!run;
                break;
            }
            //a前一帧
            case 97:
            {
                __currentFrame = std::max(0, __currentFrame - 1);
                run=false;
                step=true;
                break;
            }
            //d后一帧
            case 100:
            {
                __currentFrame = std::max(0, __currentFrame + 1);
                run=false;
                step=true;
                break;
            }
        }
        if(run)
        {
            __currentFrame++;
            __last_wait_time=__wait_time;
        }
     }
}