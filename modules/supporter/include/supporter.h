#pragma once

#include <iostream>
#include <opencv2/opencv.hpp>
#include <string.h>
#include "processor.h"
class supporter
{
protected:
    std::string __path;
    cv::Mat __init_img;
    cv::Mat __output_img;
    processor_ptr __processor;
public:
    supporter(std::string path,processor_ptr processor)
    {
        __path=path;
        __processor=processor;
    };

    virtual void start()=0;

    virtual ~supporter(){};
protected:
    inline void setPath(std::string path){this->__path=path;}
    inline void setProcessor(processor_ptr processor){this->__processor=processor;}
};

using supporter_ptr = std::shared_ptr<supporter>;
