#pragma once
#include "supporter.h"

class video : public supporter
{
private:
    int  __wait_time;
    int  __last_wait_time;
    int  __currentFrame;
    bool step;
    bool run;
public:
    video(std::string path,processor_ptr processor);

    virtual void start() override;

    virtual ~video()
    {};

};

using video_ptr = std::shared_ptr<video>;