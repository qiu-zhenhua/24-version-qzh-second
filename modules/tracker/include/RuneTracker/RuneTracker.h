/**
 * @file RuneTracker.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 神符追踪器头文件
 * @version 1.0
 * @date 2021-09-21
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include "tracker.h"
#include "Rune.h"

// 神符追踪器
class RuneTracker : public tracker
{
private:
    int __round; // 圈数

public:
    RuneTracker(combo_ptr pRune = nullptr); // 使用装甲板构造追踪器
    void update(combo_ptr) override;        // 更新时间序列
    bool isVanish() override;               // 是否已掉帧过多

protected:
    void updateFilter() override; // 更新距离滤波器

private:
    void calculateRealAngle(); // 获取考虑圈数的角度值
};
