/**
 * @file ArmorTracker.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 装甲板追踪器头文件
 * @version 1.0
 * @date 2021-08-21
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include "tracker.h"
#include "Armor.h"

// 装甲板追踪器
class ArmorTracker : public tracker
{
public:
    ArmorTracker(combo_ptr pArmor = nullptr) : tracker(pArmor) {} // 使用装甲板构造追踪器
    void update(combo_ptr) override;                              // 更新时间序列
    bool isVanish() override;                                     // 是否已掉帧过多

protected:
    void updateFilter() override; // 更新距离滤波器
};
