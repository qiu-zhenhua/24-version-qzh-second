/**
 * @file tracker.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 追踪器抽象头文件
 * @version 1.0
 * @date 2021-08-20
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include "KalmanFilterX.hpp"
#include <deque>
#include "combo.h"

// 追踪器类
class tracker
{
protected:
    std::deque<combo_ptr> __combo_deque; // 组合体时间队列
    int __vanish_num;                    // 消失帧数
    KalmanFilter22 __distance_filter;    // 距离滤波器
    bool __is_filter_init;               // 滤波器初始化标志位
    float __distance;                    // 滤波之后的目标距离

    // detect
    bool __is_tracked; // 是否为目标追踪器
    bool __is_matched; // 每一帧追踪器是否被匹配到
    uint8_t __type;    // 追踪器的类型

    // compensate
    cv::Point2f __compensate; // 补偿角度

    // predict
    bool __is_predict;            // 追踪器是否绑定预测器
    float __time;                 // 预测量 (时间信息)
    cv::Point3f __speed;          // 速度
    cv::Point3f __predict_angle;  // 预测角度
    cv::Point2f __predict_center; // 预测点

public:
    tracker(combo_ptr = nullptr);
    combo_ptr operator[](size_t) const; // 重载 [] 运算符

    virtual void update(combo_ptr) = 0; // 更新追踪器
    virtual bool isVanish() = 0;        // 是否已掉帧过多

    // 获取时间队列中最新的组合体
    inline combo_ptr getFront() { return __combo_deque.front(); }
    // 获取时间队列中最后的组合体
    inline combo_ptr getBack() { return __combo_deque.back(); }
    // 获取距离信息
    inline float getDistance() { return __distance; }
    // 设置追踪信息
    inline void setTrack(bool &&track) { __is_tracked = track; }
    // 获取追踪信息
    inline bool isTracked() { return __is_tracked; }
    // 获取掉帧数
    inline int getVanishNumber() { return __vanish_num; }
    // 获取序列数量信息
    inline size_t getSize() { return __combo_deque.size(); }
    // 设置每一帧追踪器的匹配信息
    inline void setMatchMessage(bool &&match) { __is_matched = match; }
    // 获取每一帧追踪器的匹配信息
    inline bool getMatchMessage() { return __is_matched; }
    // 获取追踪器类型
    inline uint8_t getType() { return __type; }

    // 设置补偿角度
    inline void setCompensate(const cv::Point2f &com) { __compensate = com; }
    // 获取补偿角度
    inline cv::Point2f getCompensate() { return __compensate; }

    // 设置与预测器的绑定信息
    inline void setPredictMessage(bool &&predict) { __is_predict = predict; }
    // 获取与预测器的绑定信息
    inline bool getPredictMessage() { return __is_predict; }
    // 设置时间信息预测量
    inline void setTime(float t) { __time = t; }
    // 获取时间信息预测量
    inline float getTime() { return __time; }
    // 设置预测角度
    inline void setPredictedAngle(cv::Point3f &&p3f) { __predict_angle = p3f; }
    // 获取预测角度
    inline cv::Point3f getPredictedAngle() { return __predict_angle; }
    // 设置预测点
    inline void setPredictedCenter(cv::Point2f &&p2f) { __predict_center = p2f; }
    // 获取预测点
    inline cv::Point2f getPredictedCenter() { return __predict_center; }
    // 设置速度
    inline void setSpeed(const cv::Point3f &s) { __speed = s; }
    // 获取速度
    inline cv::Point3f getSpeed() { return __speed; }

    // 索引
    inline combo_ptr at(size_t __n) const { return __combo_deque[__n]; }

protected:
    virtual void updateFilter() = 0; // 更新距离滤波器
};

using tracker_ptr = std::shared_ptr<tracker>;
