/**
 * @file tracker.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 追踪器
 * @version 1.0
 * @date 2021-08-20
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "tracker.h"
#include "opencv2/core/matx.hpp"

using namespace cv;

/**
 * @brief 使用组合体创建新追踪器
 * 
 * @param pFirstCombo 首个组合体共享指针
 */
tracker::tracker(combo_ptr pFirstCombo)
{
    // init
    __vanish_num = 0;
    __is_tracked = false;
    __is_matched = false;
    __is_predict = false;
    __is_filter_init = false;
    __distance = pFirstCombo->getPNP().distance;
    __distance_filter = KalmanFilter22(0.1f, 0.1f);
    __distance_filter.setH(Matx22f::eye());
    // 记录首帧装甲板
    if (pFirstCombo != nullptr)
    {
        __combo_deque.push_front(pFirstCombo);
    }
}

/**
 * @brief 对 tracker 中包含数据的下标访问。
 * 
 * @param __n 要访问其数据的元素的索引
 * @return combo_ptr 
 */
combo_ptr tracker::operator[](size_t __n) const
{
    return __combo_deque[__n];
}
