/**
 * @file RuneTracker.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 神符追踪器
 * @version 1.0
 * @date 2021-09-21
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "RuneTracker.h"

using namespace cv;

RuneTracker::RuneTracker(combo_ptr pRune) : tracker(pRune)
{
    __round = 0;
}

/**
 * @brief 更新时间序列
 * 
 * @param pRune 神符共享指针
 */
void RuneTracker::update(combo_ptr pRune)
{
    //若为空则记录消失帧数+1，不加入时间序列中
    if (pRune == nullptr)
    {
        __vanish_num++;
        return;
    }
    else
    {
        __vanish_num = 0;
        pRune->setMatchMessage(true);
    }
    // 保持时间序列最大长度
    if (__combo_deque.size() >= 12U)
    {
        // 从后 pop
        __combo_deque.pop_back();
    }
    // 从前 push
    __combo_deque.push_front(pRune);
    // 获取考虑圈数的角度
    this->calculateRealAngle();
    // 更新距离滤波器
    this->updateFilter();
}

/**
 * @brief 是否已掉帧过多
 * 
 * @return true 
 * @return false 
 */
bool RuneTracker::isVanish()
{
    return __vanish_num > 3;
}

/**
 * @brief 更新距离滤波器
 */
void RuneTracker::updateFilter()
{
    __distance = __combo_deque.front()->getPNP().distance;
    static float last_distance = 0.f;
    if (__is_filter_init)
    {
        // 设置状态转移矩阵
        __distance_filter.setA(Matx22f{1, 0,
                                       0, 1});
        float delta_distance = __distance - last_distance;
        // 预测
        __distance_filter.predict();
        // 更新
        Matx21f correct_vec = __distance_filter.correct({__distance,
                                                         delta_distance});
        __distance = correct_vec(0);
    }
    else
    {
        Matx21f init_vec = {__distance, 0};
        __distance_filter.init(init_vec, 1e-2);
        __is_filter_init = true;
    }
    last_distance = __distance;
}

/**
 * @brief 获取考虑圈数的角度值
 *
 * @param current_angle 当前角度值
 */
void RuneTracker::calculateRealAngle()
{
    if (__combo_deque.size() < 2)
    {
        return;
    }
    // 角度范围修正至 (-180, 180]
    float last_angle = __combo_deque[1]->getAngle();
    float current_angle = __combo_deque.front()->getAngle();
    while (fabs(last_angle) > 180.f)
    {
        last_angle += (last_angle > 0.f) ? -360.f : 360.f;
    }
    while (fabs(current_angle) > 180.f)
    {
        current_angle += (current_angle > 0.f) ? -360.f : 360.f;
    }
    // 角度判断，计算圈数
    if (current_angle > 135.f && last_angle < -135.f)
    {
        __round--;
    }
    else if (current_angle < -135.f && last_angle > 135.f)
    {
        __round++;
    }
    // 更新角度
    __combo_deque.front()->setAngle(current_angle + 360.f * __round);
}
