/**
 * @file ArmorTracker.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 装甲板追踪器
 * @version 1.0
 * @date 2021-08-21
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "ArmorTracker.h"
#include "StrategyParam.h"

using namespace cv;

/**
 * @brief 更新时间序列
 * 
 * @param pArmor 装甲板共享指针
 */
void ArmorTracker::update(combo_ptr pArmor)
{
    //若为空则记录消失帧数+1，不加入时间序列中
    if (pArmor == nullptr)
    {
        __vanish_num++;
        return;
    }
    else
    {
        __vanish_num = 0;
        pArmor->setMatchMessage(true);
    }
    // 保持时间序列最大长度
    if (this->__combo_deque.size() >= 12U)
    {
        // 从后 pop
        this->__combo_deque.pop_back();
    }
    // 从前 push
    this->__combo_deque.push_front(pArmor);
    // 更新距离滤波器
    this->updateFilter();
}

/**
 * @brief 是否已掉帧过多
 * 
 * @return true 
 * @return false 
 */
bool ArmorTracker::isVanish()
{
    return this->__vanish_num > strategy_param.NORMAL_TRACK_FRAMES;
}

/**
 * @brief 更新距离滤波器
 */
void ArmorTracker::updateFilter()
{
    this->__distance = this->__combo_deque.front()->getPNP().distance;
    static float last_distance = 0.f;
    if (this->__is_filter_init)
    {
        // 设置状态转移矩阵
        this->__distance_filter.setA(Matx22f{1, 0,
                                             0, 1});
        float delta_distance = this->__distance - last_distance;
        // 预测
        this->__distance_filter.predict();
        // 更新
        Matx21f correct_vec = __distance_filter.correct({this->__distance,
                                                         delta_distance});
        this->__distance = correct_vec(0);
    }
    else
    {
        Matx21f init_vec = {this->__distance, 0};
        this->__distance_filter.init(init_vec, 1e-2);
        this->__is_filter_init = true;
    }
    last_distance = this->__distance;
}
