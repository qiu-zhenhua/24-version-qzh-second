# add the abstract library: combination
aux_source_directory(src COMBINATION_DIRS)
add_library(combination SHARED ${COMBINATION_DIRS})
target_include_directories(combination PUBLIC include)
target_link_libraries(combination feature)

if(ARMOR)
    aux_source_directory(src/Armor ARMOR_DIRS)
    add_library(Armor SHARED ${ARMOR_DIRS})
    target_include_directories(Armor PUBLIC include/Armor)
    target_link_libraries(Armor combination LightBlob)
endif()

if(RUNE)
    aux_source_directory(src/Rune RUNE_DIRS)
    add_library(Rune SHARED ${RUNE_DIRS})
    target_include_directories(Rune PUBLIC include/Rune)
    target_link_libraries(Rune combination RuneFeature)
endif()
