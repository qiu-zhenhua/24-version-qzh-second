/**
 * @file combo.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 特征组合类头文件
 * @version 1.0
 * @date 2021-08-10
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */
#pragma once

#include "Math.h"
#include "feature.h"

struct ResultPnP;

// 特征组合类
class combo
{
protected:
    feature_ptr __primary;   // 第一轮廓
    feature_ptr __secondary; // 第二轮廓
    feature_ptr __tertiary;  // 第三轮廓

    float __combo_height;               // 组合体高度
    float __combo_width;                // 组合体宽度
    float __combo_angle;                // 组合体角度
    float __combo_ratio;                // 组合体宽高比
    float __distance;                   // 特征的间距
    bool __is_combo;                    // 是否为合适组合体
    cv::Point2f __combo_center;         // 组合体中心点
    cv::Point2f __relative_angle;       // 图像坐标系中的角度
    cv::Point2f __gyro_angle;           // 当前时刻陀螺仪的角度
    std::vector<cv::Point2f> __corners; // 组合体轮廓的角点

private:
    ResultPnP __pnp_data; // PnP 姿态解算信息
    int64 __appear_time;  // 该组合体出现的时间 -- 作为时间序列的元素
    bool __is_matched;    // 是否匹配进时间序列

public:
    combo();

    // 获取第一轮廓
    inline feature_ptr getPrimary() { return __primary; }
    // 获取第二轮廓
    inline feature_ptr getSecondary() { return __secondary; }
    // 获取第三轮廓
    inline feature_ptr getTertiary() { return __tertiary; }

    // 设置组合体高度
    inline void setHeight(float h) { __combo_height = h; }
    // 获取组合体高度
    inline float getHeight() { return __combo_height; }
    // 设置组合体宽度
    inline void setWidth(float w) { __combo_width = w; }
    // 获取组合体宽度
    inline float getWidth() { return __combo_width; }
    // 设置组合体角度
    inline void setAngle(float angle) { __combo_angle = angle; }
    // 获取组合体角度
    inline float getAngle() { return __combo_angle; }
    // 设置组合体宽高比
    inline void setRatio(float ratio) { __combo_ratio = ratio; }
    // 获取组合体宽高比
    inline float getRatio() { return __combo_ratio; }
    // 获取特征的间距
    inline float getFeatureDis() { return __distance; }
    // 获取组合体中心
    inline cv::Point2f getCenter() { return __combo_center; }
    // 获取组合体角点
    inline std::vector<cv::Point2f> getCorners() { return __corners; }
    // 获取pnp信息
    inline ResultPnP getPNP() { return __pnp_data; }
    // 设置pnp信息
    inline void setPNP(const ResultPnP &pnp) { __pnp_data = pnp; }
    // 设置pnp信息
    inline void setPNP(ResultPnP &&pnp) { __pnp_data = pnp; }
    // 获取时间戳
    inline int64 getTime() { return __appear_time; }
    // 设置时间戳
    inline void setTime(const int64 &t) { __appear_time = t; }
    // 是否为合适组合体
    inline bool isCombo() { return __is_combo; }
    // 获得组合体在时间序列中的匹配情况
    inline bool getMatchMessage() { return __is_matched; }
    // 设置组合体在时间序列中的匹配情况
    inline void setMatchMessage(bool &&match) { __is_matched = match; }
    // 获得组合体在图像坐标系下的角度
    inline cv::Point2f getRelativeAngle() { return __relative_angle; }
    // 获得当前时刻陀螺仪的角度
    inline cv::Point2f getGyroAngle() { return __gyro_angle; }
};

// 组合体共享指针
using combo_ptr = std::shared_ptr<combo>;
