/**
 * @file Rune.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 神符类头文件
 * @version 1.0
 * @date 2021-09-11
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * Primary
 */

#pragma once

#include "combo.h"
#include "RuneArmor.h"
#include "RuneCenter.h"

// 神符类
class Rune : public combo
{
    // pPrimary <- 神符装甲板
    // pSencondary <- 神符旋转中心

private:
    bool __is_active; // 是否激活

public:
    Rune(RuneA_ptr &, RuneC_ptr &);

    // 是否激活
    inline bool isActive() { return __is_active; }

private:
    bool isSuitableRune(RuneA_ptr, RuneC_ptr);                              // 是否为合适的神符扇叶
    float calculateRotatedAngle(RuneA_ptr, RuneC_ptr);                      // 计算神符旋转角度
    ResultPnP calculatePnPData(RuneA_ptr, RuneC_ptr, cv::Mat &, cv::Mat &); // 计算神符装甲板的位姿
    std::vector<cv::Point2f> calculatePoints(RuneA_ptr, RuneC_ptr);         // 按照左下，左上，右上，右下的顺序，计算神符装甲板的四个角点
};

using Rune_ptr = std::shared_ptr<Rune>;
