/**
 * @file Armor.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 装甲板类头文件
 * @version 1.0
 * @date 2021-08-13
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "combo.h"
#include "LightBlob.h"

// 装甲板类
class Armor : public combo
{
public:
private:
    // 装甲板种类
    enum class ArmorType
    {
        SMALL,
        BIG
    };

    float __length_ratio;      // 左右灯条长度比
    float __width_ratio;       // 左右灯条宽度比
    float __angle_diff;        // 左右灯条夹角
    float __dislocation_ratio; // 灯条错位比值
    float __match_error;       // 匹配误差
    bool __can_erace;          // 是否允许被删除的标志
    ArmorType __armor_type;    // 装甲板类型

public:
    Armor(Blob_ptr, Blob_ptr, const cv::Point2f &);

    // 设置是否允许被删除
    inline void setEraseMessage(bool &&erase) { __can_erace = erase; }
    // 获取是否允许被删除
    inline bool getEraseMessage() { return __can_erace; }
    // 获取匹配误差
    inline float getError() { return __match_error; }

private:
    ResultPnP calculatePnPData(Blob_ptr, Blob_ptr, cv::Mat &, cv::Mat &); // 获取装甲板的位姿
    ArmorType matchArmorType();                                           // 用来确定装甲板的种类 (大装甲或者小装甲)
    bool isSuitable(Blob_ptr, Blob_ptr);                                  // 判断组合体是否合适

    // 计算装甲板中心
    inline cv::Point2f calculateCenter(Blob_ptr l, Blob_ptr r) { return (l->getCenter() + r->getCenter()) / 2.f; }
};

using Armor_ptr = std::shared_ptr<Armor>;
