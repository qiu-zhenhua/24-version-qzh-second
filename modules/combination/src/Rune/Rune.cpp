/**
 * @file Rune.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 神符类
 * @version 1.0
 * @date 2021-09-11
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "Rune.h"
#include "CameraParam.h"
#include "fixed.h"
#include "RuneParam.h"

using namespace std;
using namespace cv;

/**
 * @brief 有参构造函数
 * 
 * @param _pArmor 神符装甲板共享指针
 * @param _pCenter 神符旋转中心共享指针
 */
Rune::Rune(RuneA_ptr &_pArmor, RuneC_ptr &_pCenter) : combo()
{
    if (isSuitableRune(_pArmor, _pCenter))
    {
        // 获取神符中心
        __combo_center = _pArmor->getCenter();
        // 更新激活信息
        __is_active = _pArmor->isActive();
        // 更新神符角度
        __combo_angle = this->calculateRotatedAngle(_pArmor, _pCenter);
        // 计算 PnP 信息
        this->setPNP(calculatePnPData(_pArmor, _pCenter,
                                      camera_param.cameraMatrix,
                                      camera_param.distCoeff));
        // 设置组合体特征指针
        __primary = _pArmor;
        __secondary = _pCenter;
        __tertiary = nullptr;
        // 更新标志位
        __is_combo = true;
    }
}

/**
 * @brief 是否为合适的神符扇叶
 * 
 * @param pArmor 神符装甲板
 * @param pCenter 神符旋转中心
 */
bool Rune::isSuitableRune(RuneA_ptr pArmor, RuneC_ptr pCenter)
{
    // ------------------------------【判空】------------------------------
    if (pArmor == nullptr || pCenter == nullptr)
        return false;

    // ------------------------【是否为合适的特征】------------------------
    if (!pArmor->isFeature() || !pCenter->isFeature())
        return false;

    // ----------------------【特征面积之比是否合适】----------------------
    // if (pArmor->getArea() / pCenter->getArea() < rune_param.MIN_AREA_RATIO)
    // {
    //     return false;
    // }

    // ------------------------【特征间距是否合适】------------------------
    // 使用神符装甲板的长宽信息代表组合体的长宽信息
    __combo_width = pArmor->getWidth();
    __combo_height = pArmor->getHeight();
    __distance = getDistances(pArmor->getCenter(), pCenter->getCenter());
    if (__distance / __combo_height < rune_param.MIN_FEATURE_DISTANCE ||
        __distance / __combo_height > rune_param.MAX_FEATURE_DISTANCE)
    {
        return false;
    }
    return true;
}
