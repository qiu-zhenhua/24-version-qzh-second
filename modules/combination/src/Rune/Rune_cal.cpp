/**
 * @file Rune_cal.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 神符类中有关计算内容的成员方法实现
 * @version 1.0
 * @date 2021-09-13
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include <eigen3/Eigen/Dense>
#include <opencv2/core/eigen.hpp>
#include <opencv2/calib3d.hpp>
#include "fixed.h"
#include "Rune.h"

using namespace std;
using namespace cv;
using namespace Eigen;

/**
 * @brief 获取神符旋转角度
 * 
 * @param pArmor 神符装甲板
 * @param pCenter 神符旋转中心
 */
float Rune::calculateRotatedAngle(RuneA_ptr pArmor, RuneC_ptr pCenter)
{
    float delta_x = pArmor->getCenter().x - pCenter->getCenter().x;
    float delta_y = pCenter->getCenter().y - pArmor->getCenter().y;
    float angle = 0.f;
    // 范围 (-180, 180]
    if (delta_x == 0.f)
    {
        angle = (delta_y > 0) ? 90.f : -90.f;
    }
    else
    {
        angle = rad2deg(atanf(delta_y / delta_x));
        if (delta_x < 0.f)
        {
            angle = (angle > 0) ? angle - 180.f : angle + 180.f;
        }
    }
    return angle;
}

/**
 * @brief 计算神符装甲板的位姿
 * 
 * @param pArmor 神符装甲板
 * @param pCenter 神符旋转中心
 * @param cameraMatrix 相机内参矩阵，用于解算 pnp
 * @param distcoeff 相机畸变参数，用于解算 pnp
 * @return ResultPnP 神符装甲板的位姿
 */
ResultPnP Rune::calculatePnPData(RuneA_ptr pArmor, RuneC_ptr pCenter, Mat &cameraMatrix, Mat &distcoeff)
{
    // 像素坐标系下的坐标点
    __corners.clear();
    __corners = this->calculatePoints(pArmor, pCenter);
    Mat rvec;              // 旋转向量
    Mat tvec;              // 平移向量
    ResultPnP pnp_message; // 存储 PNP 信息
    // 姿态解算
    solvePnP(wRUNE_CONTOUR, __corners, cameraMatrix, distcoeff, rvec, tvec, false, SOLVEPNP_IPPE);
    Mat rmat; // 旋转矩阵
    // 罗德里格斯变换: 旋转向量 -> 旋转矩阵
    Rodrigues(rvec, rmat);
    rmat.convertTo(rmat, CV_64FC1);
    tvec.convertTo(tvec, CV_64FC1);
    Matrix3d rotated_matrix;  // 旋转矩阵
    Vector3d transfer_vector; // 平移向量
    // 类型转换
    cv2eigen(rmat, rotated_matrix);
    cv2eigen(tvec, transfer_vector);
    // 获取欧拉角
    Vector3d euler_angles = rotated_matrix.eulerAngles(0, 1, 2);

    // PNP 角度信息
    pnp_message.pitch = rad2deg(euler_angles[0]);
    pnp_message.yaw = rad2deg(euler_angles[1]);
    pnp_message.roll = rad2deg(euler_angles[2]);
    // PNP 距离信息
    pnp_message.distance = sqrt(transfer_vector.transpose() * transfer_vector);

    tvec.convertTo(tvec, CV_32FC1);
    pnp_message.tran_vec = tvec;
    pnp_message.rotat_vec = rvec;

    return pnp_message;
}
/**
 * @brief 计算神符装甲板的四个角点
 * 
 * @param pArmor 神符装甲板
 * @param pCenter 神符旋转中心
 * @return 按照左下，左上，右上，右下的顺序排列的角点向量
 */
vector<Point2f> Rune::calculatePoints(RuneA_ptr pArmor, RuneC_ptr pCenter)
{
    // -------------------------------【获取神符四个角点】-------------------------------
    // 四个角点
    vector<Point2f> four_corners = pArmor->getCorners();

    // 按照离神符中心远近升序角点
    sort(four_corners.begin(), four_corners.end(),
         [&](const Point2f &p1, const Point2f &p2)
         {
             return getDistances(p1, pCenter->getCenter()) < getDistances(p2, pCenter->getCenter());
         });
    // 存储靠近神符中心的角点
    Point2f bottom_1 = four_corners[0];
    Point2f bottom_2 = four_corners[1];
    // 存储远离神符中心的角点
    Point2f top_1 = four_corners[2];
    Point2f top_2 = four_corners[3];

    // ------------【判断角度大小来获取相对于神符装甲板中心左角点 or 右角点】------------
    // bottom_angle
    float bottom_delta_angle = getHorizontalAngle(bottom_1, pCenter->getCenter()) -
                               getHorizontalAngle(bottom_2, pCenter->getCenter());
    // 角度弧度制转换
    bottom_delta_angle = rad2deg(bottom_delta_angle);
    // 得到 bottom_left / right
    if (bottom_delta_angle < -90.f)
    {
        bottom_delta_angle += 180.f;
    }
    else if (bottom_delta_angle > 90.f)
    {
        bottom_delta_angle -= 180.f;
    }

    Point2f bottom_left = (bottom_delta_angle > 0.f) ? bottom_1 : bottom_2;
    Point2f bottom_right = (bottom_delta_angle < 0.f) ? bottom_1 : bottom_2;
    // top_angle
    float top_delta_angle = getHorizontalAngle(top_1, pCenter->getCenter()) -
                            getHorizontalAngle(top_2, pCenter->getCenter());
    // 角度弧度制转换
    top_delta_angle = rad2deg(top_delta_angle);
    // 得到 top_left / right
    if (top_delta_angle < -90.f)
    {
        top_delta_angle += 180.f;
    }
    else if (top_delta_angle > 90.f)
    {
        top_delta_angle -= 180.f;
    }

    Point2f top_left = (top_delta_angle > 0.f) ? top_1 : top_2;
    Point2f top_right = (top_delta_angle < 0.f) ? top_1 : top_2;
    // 4 神符装甲板角点 + 1 神符中心点
    vector<Point2f> img_points = {bottom_left, top_left, top_right, bottom_right, pCenter->getCenter()};
    return img_points;
}
