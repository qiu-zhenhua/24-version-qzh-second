/**
 * @file Armor.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 装甲板类
 * @version 1.0
 * @date 2021-08-13
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "Armor.h"
#include "ArmorParam.h"
#include "LightBlobParam.h"
#include "CameraParam.h"

using namespace std;
using namespace cv;

/**
 * @brief 使用左右灯条构造装甲板
 * 
 * @param pLeft 左灯条
 * @param pRight 右灯条
 * @param gyro_location 陀螺仪位置信息
 */
Armor::Armor(Blob_ptr pLeft, Blob_ptr pRight, const Point2f &gyro_location) : combo()
{
    // init
    this->__length_ratio = 0.f;
    this->__width_ratio = 0.f;
    this->__angle_diff = 0.f;
    this->__can_erace = false;
    // 判断是否符合
    if (this->isSuitable(pLeft, pRight))
    {
        // 获取装甲板中心
        __combo_center = this->calculateCenter(pLeft, pRight);
        // 更新装甲板角度
        __combo_angle = (pLeft->getAngle() + pRight->getAngle()) / 2;
        // 匹配大小装甲板
        this->__armor_type = this->matchArmorType();
        // 计算 PnP 信息
        this->setPNP(this->calculatePnPData(pLeft, pRight, camera_param.cameraMatrix, camera_param.distCoeff));
        // 获取相对角度
        this->__relative_angle = calculateRelativeAngle(camera_param.cameraMatrix,
                                                        camera_param.distCoeff,
                                                        this->__combo_center);
        // 获取当前装甲板对应的陀螺仪位置信息
        this->__gyro_angle = gyro_location;
        // 更新灯条匹配标志位
        pLeft->setMatchMessage(true);
        pRight->setMatchMessage(true);
        // 设置组合体特征指针
        __primary = pLeft;
        __secondary = pRight;
        __tertiary = nullptr;
        // 更新标志位
        this->__is_combo = true;
    }
}

/**
 * @brief 判断组合体是否合适
 * 
 * @param pLeft 左灯条
 * @param pRight 右灯条
 * @return 组合体是否合适
 */
bool Armor::isSuitable(Blob_ptr pLeft, Blob_ptr pRight)
{
    // ------------------------------【判空】------------------------------
    if (pLeft == nullptr || pRight == nullptr)
        return false;

    // ------------------------【是否为合适的特征】------------------------
    if (!pLeft->isFeature() || !pRight->isFeature())
        return false;

    // --------------------------【面积是否符合】--------------------------
    if (pLeft->getArea() < light_blob_param.MIN_AREA ||
        pRight->getArea() < light_blob_param.MIN_AREA)
    {
        return false;
    }

    // --------------------【左右灯条长度比值是否符合】--------------------
    float length_l = pLeft->getHeight();
    float length_r = pRight->getHeight();
    // 获取长度偏差
    this->__length_ratio = (length_l / length_r >= 1) ? length_l / length_r : length_r / length_l;
    // cout << "length_ratio = " << this->__length_ratio << endl;
    if (this->__length_ratio >= armor_param.MAX_LENGTH_RATIO)
    {
        return false;
    }

    // --------------------【左右灯条宽度比值是否符合】--------------------
    float width_l = pLeft->getWidth();
    float width_r = pRight->getWidth();
    // 获取宽度偏差
    this->__width_ratio = (width_l / width_r >= 1) ? width_l / width_r : width_r / width_l;
    // cout << "width_ratio = " << this->__width_ratio << endl;
    if (this->__width_ratio >= armor_param.MAX_WIDTH_RATIO)
    {
        return false;
    }

    // ------------------------【角度差值是否符合】------------------------
    //获取角度偏差
    float angle_scale = fabs(pLeft->getAngle() - pRight->getAngle());
    // cout << "angle_scale = " << angle_scale << endl;
    if (angle_scale >= armor_param.MAX_DELTA_ANGLE)
    {
        return false;
    }

    // ---------------------【装甲板长宽比值是否符合】---------------------
    this->setHeight((length_l + length_r) / 2);
    // 灯条间距即为装甲板的宽
    this->__distance = getDistances(pLeft->getCenter(), pRight->getCenter());
    this->setWidth(__distance);
    this->setRatio(this->__combo_width / this->__combo_height);
    if (this->__combo_ratio <= armor_param.MIN_DISTANCE_LENGTH_RATIO ||
        this->__combo_ratio >= armor_param.MAX_DISTANCE_LENGTH_RATIO)
    {
        return false;
    }

    // --------------------【装甲板对角线长度是否符合】--------------------
    float cross_tl2br = getDistances(pLeft->getTopPoint(), pRight->getBottomPoint());
    float cross_bl2tr = getDistances(pLeft->getBottomPoint(), pRight->getTopPoint());
    float max_height = max(length_l, length_r);
    if (cross_tl2br < armor_param.MIN_CROSS_RATIO * max_height ||
        cross_bl2tr < armor_param.MIN_CROSS_RATIO * max_height)
    {
        return false;
    }

    // -------------------【左右灯条投影长度是否符合】---------------------
    float center_distance = getDistances(pLeft->getCenter(), pRight->getCenter());
    float center_y_distance = fabs(pLeft->getCenter().y - pRight->getCenter().y);
    // 计算两灯条中心点对灯条的夹角
    float angle_x = rad2deg(getHorizontalAngle(pLeft->getCenter(), pRight->getCenter()));
    float angle_y = (pLeft->getAngle() + pRight->getAngle()) / 2;
    float delta_angle = 90.f - angle_x - angle_y;

    float min_height = min(length_r, length_l);
    // 沿灯条方向投影长度比值
    this->__dislocation_ratio = fabs(center_distance * cosf(deg2rad(delta_angle))) / min_height;
    // y 方向投影长度比值
    float dislocation_y_ratio = center_y_distance / min_height;
    if (this->__dislocation_ratio >= armor_param.MAX_UP_DOWN_DIS_RATIO ||
        dislocation_y_ratio >= armor_param.MAX_UP_DOWN_DIS_RATIO)
    {
        return false;
    }

    // --------------------------【更新匹配误差】--------------------------
    this->__match_error = armor_param.ERROR_LENGTH_SCALE_RATIO * this->__length_ratio +
                          armor_param.ERROR_WIDTH_SCALE_RATIO * this->__width_ratio +
                          armor_param.ERROR_ANGLE_SCALE_RATIO * this->__angle_diff +
                          armor_param.ERROR_DISLOCATION_RATIO * this->__dislocation_ratio +
                          armor_param.ERROR_ARMOR_SCALE_RATIO * this->__combo_ratio;
    return true;
}