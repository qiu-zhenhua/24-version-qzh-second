/**
 * @file Armor_cal.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 装甲板类中有关计算内容的成员方法实现
 * @version 1.0
 * @date 2021-08-17
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "Armor.h"
#include <eigen3/Eigen/Dense>
#include <opencv2/core/eigen.hpp>
#include <opencv2/calib3d.hpp>
#include "ArmorParam.h"
#include "fixed.h"

using namespace std;
using namespace cv;
using namespace Eigen;

/**
 * @brief 用来确定装甲板的种类 (大装甲或者小装甲)
 * 
 * @param camera_param 相机参数
 * @return  
 */
Armor::ArmorType Armor::matchArmorType()
{
    // 装甲板长宽比例符合
    if (this->__combo_ratio < armor_param.SCALE_THRESHOLD)
    {
        // 是否倾斜
        if (this->__width_ratio < armor_param.WIDTH_SCALE_THRESHOLD)
        {
            // 倾斜程度不高则匹配为小装甲板
            return ArmorType::SMALL;
        }
        else
        {
            // 倾斜时进一步比较长宽比例
            if (this->__combo_ratio < armor_param.TILT_SCALE_THRESHOLD)
            {
                // 装甲板长宽比例较小
                return ArmorType::SMALL;
            }
            else
            {
                return ArmorType::BIG;
            }
        }
    }
    else
    {
        return ArmorType::BIG;
    }
}

/**
 * @brief 获取装甲板的位姿
 * 
 * @param pLeft 左灯条共享指针
 * @param pRight 右灯条共享指针
 * @param cameraMatrix 相机内参，用于解算pnp
 * @param distcoeff 相机畸变参数，用于解算pnp
 * 
 * @return ResultPnP - PnP 姿态解算信息
 */
ResultPnP Armor::calculatePnPData(Blob_ptr pLeft, Blob_ptr pRight, Mat &cameraMatrix, Mat &distcoeff)
{
    // 像素坐标系下的坐标点
    __corners.push_back(pLeft->getBottomPoint());  // 左下
    __corners.push_back(pLeft->getTopPoint());     // 左上
    __corners.push_back(pRight->getTopPoint());    // 右上
    __corners.push_back(pRight->getBottomPoint()); // 右下

    Mat rvec;              // 旋转向量
    Mat tvec;              // 平移向量
    ResultPnP pnp_message; // 存储 PNP 信息

    if (__armor_type == ArmorType::SMALL)
    {
        solvePnP(wSMALL_ARMOR, __corners, cameraMatrix, distcoeff, rvec, tvec, false, SOLVEPNP_IPPE);
    }
    else
    {
        solvePnP(wBIG_ARMOR, this->__corners, cameraMatrix, distcoeff, rvec, tvec, false, SOLVEPNP_IPPE);
    }

    Mat rmat; // 旋转矩阵
    // 罗德里格斯变换: 旋转向量 -> 旋转矩阵
    Rodrigues(rvec, rmat);
    rmat.convertTo(rmat, CV_64FC1);
    tvec.convertTo(tvec, CV_64FC1);
    Matrix3d rotated_matrix;  // 旋转矩阵
    Vector3d transfer_vector; // 平移向量
    // 类型转换
    cv2eigen(rmat, rotated_matrix);
    cv2eigen(tvec, transfer_vector);
    // 获取欧拉角
    Vector3d euler_angles = rotated_matrix.eulerAngles(0, 1, 2);

    pnp_message.pitch = rad2deg(euler_angles[0]);
    pnp_message.yaw = rad2deg(euler_angles[1]);
    pnp_message.roll = rad2deg(euler_angles[2]);

    if (this->__armor_type == ArmorType::SMALL)
    {
        pnp_message.distance = cosf(armor_param.SMALL_COEFF_W * deg2rad(this->__combo_angle)) *
                               sqrt(transfer_vector.transpose() * transfer_vector);
    }
    else
    {
        pnp_message.distance = cosf(armor_param.BIG_COEFF_W * deg2rad(this->__combo_angle)) *
                               sqrt(transfer_vector.transpose() * transfer_vector);
    }

    tvec.convertTo(tvec, CV_32FC1);
    pnp_message.tran_vec = tvec;
    pnp_message.rotat_vec = rvec;

    return pnp_message;
}