/**
 * @file combo.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 特征组合类
 * @version 1.0
 * @date 2021-08-10
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "combo.h"

/**
 * @brief Construct a new combo::combo object
 * 
 */
combo::combo() : __primary(nullptr),
                 __secondary(nullptr),
                 __tertiary(nullptr),
                 __combo_height(0.f),
                 __combo_width(0.f),
                 __distance(0.f),
                 __combo_angle(0.f),
                 __combo_ratio(0.f),
                 __is_combo(false),
                 __is_matched(false)
{
}
