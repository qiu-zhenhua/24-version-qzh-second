#include "ArmorDetector.h"
#include "LightBlobParam.h"
#include "ArmorParam.h"
using namespace std;
using namespace cv;

void ArmorDetector::find(Mat &src , vector<feature_ptr> &features, vector<combo_ptr> &combos)
{
        // ----------------------- light_blob -----------------------
    // 找到所有灯条
    vector<Blob_ptr> blobs = this->findLightBlobs(src);
    // 灯条从左到右排序
    sort(blobs.begin(), blobs.end(),
         [&](const Blob_ptr &pLeft, const Blob_ptr &pRight) -> bool
         {
             return pLeft->getCenter().x < pRight->getCenter().x;
         });
    // 更新至特征容器
    for (auto blob : blobs)
    {
        features.emplace_back(blob);
    }

    // ------------------------- armor --------------------------
    if (blobs.size() >= 2)
    {
        // 找到所有装甲板
        vector<Armor_ptr> armors = this->findArmors(blobs);
        // 根据匹配误差筛选
        this->eraseErrorArmors(armors);
        // 更新至组合体容器
        for (auto armor : armors)
        {
            this->__current_combos.emplace_back(armor);
        }
    }
}

/**
 * @brief 寻找灯条
 * 
 * @param bin 二值图
 * 
 * @return 找到的灯条
 */
vector<Blob_ptr> ArmorDetector::findLightBlobs(Mat &bin)
{
    // 储存找到的灯条
    vector<Blob_ptr> light_blobs;
    // 储存查找出的轮廓
    vector<vector<Point>> contours;
    // 查找最外围轮廓
    findContours(bin, contours, RETR_EXTERNAL, CHAIN_APPROX_NONE);
    //
    for (auto &contour : contours)
    {
        // 排除面积过小的误识别
        if (contourArea(contour) < light_blob_param.MIN_AREA)
        {
            continue;
        }
        // 构造灯条对象
        Blob_ptr pLight(new LightBlob(contour));
        if (pLight->isFeature())
        {
            // 将识别出的灯条 push 到 light_blobs 中
            light_blobs.push_back(pLight);
        }
        else
        {
            // 结束共享
            pLight.reset();
        }
    }
    return light_blobs;
}

/**
 * @brief 匹配装甲板
 * 
 * @param light_blobs 利用找到的灯条匹配装甲板
 * 
 * @return 当前帧找到的所有装甲板
 */
vector<Armor_ptr> ArmorDetector::findArmors(vector<Blob_ptr> &light_blobs)
{
    // 储存所有匹配到的装甲板
    vector<Armor_ptr> current_armors;
    // -------------------------------------【匹配】-------------------------------------
    // 遍历每个灯条，向右进行匹配
    for (auto left_blob = light_blobs.begin(); left_blob != light_blobs.end() - 1; left_blob++)
    {
        // 向右寻找第一个灯条与之进行匹配
        for (auto right_blob = left_blob + 1; right_blob < light_blobs.end(); right_blob++)
        {
            // 构造装甲板
            Armor_ptr armor(new Armor(*left_blob, *right_blob, this->__gyro_location));
            // 找到目标则打入容器
            if (armor->isCombo())
            {
                // 装甲板 push 进 current_armors
                current_armors.push_back(armor);
                break;
            }
            else
            {
                // 获取左右灯条中心竖直距离与平均值
                float delta_center_y = fabs((*left_blob)->getCenter().y - (*right_blob)->getCenter().y);
                float average_height = ((*left_blob)->getHeight() + (*right_blob)->getHeight()) / 2.f;
                // 不满足极度错位条件
                if (delta_center_y / average_height < armor_param.MIN_DISLOCATION_RATIO)
                {
                    float height_ratio = (*left_blob)->getHeight() / (*right_blob)->getHeight();
                    height_ratio = (height_ratio < 1) ? (1 / height_ratio) : height_ratio;
                    // 高度差异
                    if (height_ratio < armor_param.MAX_LENGTH_RATIO)
                    {
                        // 退出右灯条的遍历
                        break;
                    }
                }
            }
        }
    }
    return current_armors;
}

/**
 * @brief 根据匹配误差筛选装甲板
 * 
 * @param armors 待筛选的所有装甲板
 */
void ArmorDetector::eraseErrorArmors(std::vector<Armor_ptr> &armors)
{
    // 判断大小是否允许被删除
    if (armors.size() < 2)
    {
        return;
    }
    // 设置是否删除的标志位
    for (auto lIt = armors.begin(); lIt != armors.end() - 1; lIt++)
    {
        for (auto rIt = lIt + 1; rIt != armors.end(); rIt++)
        {
            if ((*lIt)->getPrimary() == (*rIt)->getPrimary() ||
                (*lIt)->getPrimary() == (*rIt)->getSecondary() ||
                (*lIt)->getSecondary() == (*rIt)->getPrimary() ||
                (*lIt)->getSecondary() == (*rIt)->getSecondary())
            {
                (*lIt)->getError() > (*rIt)->getError() ? (*lIt)->setEraseMessage(true) : (*rIt)->setEraseMessage(true);
            }
        }
    }
    // 删除
    armors.erase(remove_if(armors.begin(), armors.end(),
                           [&](Armor_ptr &it)
                           {
                               return it->getEraseMessage();
                           }),
                 armors.end());
}