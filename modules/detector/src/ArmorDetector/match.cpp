#include "ArmorDetector.h"
#include "ArmorParam.h"

using namespace std;
using namespace cv;

/**
 * @brief 判断是否突变
 * 
 * @note 如果装甲板在一帧内位置变化特别大 (大于原来距离的 MAX_DELTA_DIS_RATIO 倍) ,
 *       且速度发生较大突变，则创建新序列
 * 
 * @param t_combo 时间序列中最新的装甲板
 * @param armor 待判断的装甲板
 * @param dis 测出的最小间距
 * @return 是否突变
 */
inline bool isChange(combo_ptr t_combo, combo_ptr combo, float dis)
{
    return ((t_combo->getAngle() * combo->getAngle() < -80.f) ||
            dis > armor_param.MAX_DELTA_DIS)
               ? true
               : false;
}

/**
 * @brief 匹配、更新时间序列
 * 
 * @param combos 每一帧的所有目标
 * @param record_time 当前时间戳
 */
void ArmorDetector::match(vector<combo_ptr> &combos, const int64 &record_time)
{
    // 记录时间戳
    for (auto combo : combos)
    {
        combo->setTime(record_time);
    }
    // 匹配
    this->matchArmors(combos);
    // 删除
    this->eraseNullTracker();
}

/**
 * @brief 装甲板匹配至时间序列
 * 
 * @param combos 每一帧的所有目标
 */
void ArmorDetector::matchArmors(vector<combo_ptr> &combos)
{
    // 如果 trackers 为空先为每个识别到的 combo 开辟序列
    if (this->__trackers.empty())
    {
        for (auto combo : combos)
        {
            this->__trackers.emplace_back(new ArmorTracker(combo));
        }
        return;
    }

    // 如果当前帧识别到的装甲板数量 > 序列数量
    if (combos.size() > this->__trackers.size())
    {
        // 距离最近的装甲板匹配到相应的序列中, 并 update
        for (auto tracker : this->__trackers)
        {
            // 离 tracker 最近的 combo 及其距离
            combo_ptr min_dis_combo = nullptr;
            float min_dis = MAXFLOAT;
            for (auto combo : combos)
            {
                if (!combo->getMatchMessage())
                {
                    float distance = getDistances(combo->getCenter(), tracker->getFront()->getCenter());
                    // 更新最小值
                    if (distance < min_dis)
                    {
                        min_dis_combo = combo;
                        min_dis = distance;
                    }
                }
            }
            //
            tracker->update(min_dis_combo);
            min_dis_combo->setMatchMessage(true);
        }
        // 没有匹配到的装甲板作为新的序列
        for (auto combo : combos)
        {
            if (!combo->getMatchMessage())
            {
                this->__trackers.emplace_back(new ArmorTracker(combo));
            }
        }
    }
    // 如果当前帧识别到的装甲板数量 < 序列数量
    else if (combos.size() < this->__trackers.size())
    {
        for (auto combo : combos)
        {
            // 离 armor 最近的 tracker 及其距离
            tracker_ptr min_dis_tracker = nullptr;
            float min_dis = MAXFLOAT;
            for (auto tracker : __trackers)
            {
                if (!tracker->getMatchMessage())
                {
                    float distance = getDistances(tracker->getFront()->getCenter(), combo->getCenter());
                    // 更新最小值
                    if (distance < min_dis)
                    {
                        min_dis_tracker = tracker;
                        min_dis = distance;
                    }
                }
            }
            min_dis_tracker->update(combo);
            min_dis_tracker->setMatchMessage(true);
        }
        // 循环遍历 trackers
        for (auto tracker : __trackers)
        {
            // 没有匹配到的序列传入 nullptr
            if (!tracker->getMatchMessage())
            {
                tracker->update(nullptr);
            }
            else
            {
                // 匹配到的 tracker 标志重置为 false 供下一帧循环使用
                tracker->setMatchMessage(false);
            }
        }
    }
    // 如果当前帧识别到的装甲板数量 = 序列数量
    else
    {
        for (auto tracker : __trackers)
        {
            // 离 tracker 最近的 combo 及其距离
            combo_ptr min_dis_combo = nullptr;
            float min_dis = MAXFLOAT;
            for (auto combo : combos)
            {
                if (!combo->getMatchMessage())
                {
                    float distance = getDistances(combo->getCenter(),
                                                  tracker->getFront()->getCenter());
                    if (distance < min_dis)
                    {
                        min_dis_combo = combo;
                        min_dis = distance;
                    }
                }
            }

            // 判断是否突变
            if (isChange(tracker->getFront(), min_dis_combo, min_dis))
            {
                // 创建新序列
                this->__trackers.emplace_back(new ArmorTracker(min_dis_combo));
                // 原来的序列打入 nullptr
                tracker->update(nullptr);
            }
            // 若变化不大, 正常更新
            else
            {
                tracker->update(min_dis_combo);
            }
        }
    }
}

/**
 * @brief 及时删除多帧为空的序列
 */
void ArmorDetector::eraseNullTracker()
{
    // 删除
    __trackers.erase(remove_if(__trackers.begin(), __trackers.end(),
                               [&](tracker_ptr &t1)
                               {
                                   return t1->isVanish();
                               }),
                     __trackers.end());
}