/**
 * @file match.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief match
 * @version 1.0
 * @date 2021-09-21
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "RuneDetector.h"
#include "RuneParam.h"
#include <iostream>

using namespace std;
using namespace cv;

/**
 * @brief 匹配、更新时间序列
 * 
 * @param combos 每一帧的所有目标
 * @param record_time 当前时间戳
 */
void RuneDetector::match(vector<combo_ptr> &combos, const int64 &record_time)
{
    // 记录时间戳
    for (auto combo : combos)
    {
        combo->setTime(record_time);
    }
    // 匹配
    this->matchRunes(combos);
    // 删除
    this->eraseNullTracker();
}

/**
 * @brief 神符匹配至时间序列
 * 
 * @param combos 每一帧的所有目标
 */
void RuneDetector::matchRunes(vector<combo_ptr> &combos)
{
    // 如果 trackers 为空先为每个识别到的 active_rune 开辟序列
    if (__trackers.empty())
    {
        for (auto rune : combos)
        {
            __trackers.emplace_back(make_shared<RuneTracker>(rune));
        }
        return;
    }

    // 如果当前帧识别到的激活神符数量 > 序列数量
    if (combos.size() > __trackers.size())
    {
        // 距离最近的激活神符匹配到相应序列中，并 update
        for (auto tracker : __trackers)
        {
            // 离 tracker 最近的 rune 及其角度
            float min_delta_angle = MAXFLOAT;
            combo_ptr min_delta_angle_rune = nullptr;
            for (auto rune : combos)
            {
                // 获取角度差
                float delta_angle = getDeltaAngle(rune->getAngle(), tracker->getFront()->getAngle());
                // 更新最小值
                if (delta_angle < min_delta_angle)
                {
                    min_delta_angle_rune = rune;
                    min_delta_angle = delta_angle;
                }
            }
            tracker->update(min_delta_angle_rune);
            min_delta_angle_rune->setMatchMessage(true);
        }
        // 没有匹配到的激活神符作为新的序列
        for (auto rune : combos)
        {
            if (!rune->getMatchMessage())
            {
                __trackers.emplace_back(make_shared<RuneTracker>(rune));
            }
        }
    }

    // 如果当前帧识别到的激活神符数量 < 序列数量
    else if (combos.size() < __trackers.size())
    {
        for (auto active_rune : combos)
        {
            // 离 active_rune 最近的 tracker 及其角度
            tracker_ptr min_angle_tracker = nullptr;
            float min_angle = MAXFLOAT;
            for (auto tracker : __trackers)
            {
                if (!tracker->getMatchMessage())
                {
                    // 获取角度差
                    float delta_angle = getDeltaAngle(active_rune->getAngle(),
                                                      tracker->getFront()->getAngle());
                    // 更新最小值
                    if (delta_angle < min_angle)
                    {
                        min_angle_tracker = tracker;
                        min_angle = delta_angle;
                    }
                }
            }
            min_angle_tracker->update(active_rune);
            min_angle_tracker->setMatchMessage(true);
        }
        // 循环遍历 trackers
        for (auto tracker : __trackers)
        {
            // 没有匹配到的序列传入 nullptr
            if (!tracker->getMatchMessage())
            {
                tracker->update(nullptr);
            }
            else
            {
                // 匹配到的 tracker 标志重置为 false 供下一帧循环使用
                tracker->setMatchMessage(false);
            }
        }
    }

    // 如果当前帧识别到的装甲板数量 = 序列数量
    else
    {
        for (auto tracker : __trackers)
        {
            // 离 tracker 最近的 active_rune 及其角度
            combo_ptr min_delta_angle_rune = nullptr;
            float min_delta_angle = MAXFLOAT;
            for (auto active_rune : combos)
            {
                if (!active_rune->getMatchMessage())
                {
                    // 获取角度差
                    float delta_angle = getDeltaAngle(active_rune->getAngle(),
                                                      tracker->getFront()->getAngle());
                    // 更新最小值
                    if (delta_angle < min_delta_angle)
                    {
                        min_delta_angle_rune = active_rune;
                        min_delta_angle = delta_angle;
                    }
                }
            }
            // 判断是否角度差过大
            if (min_delta_angle < 50.f)
            {
                tracker->update(min_delta_angle_rune);
            }
            else
            {
                tracker->update(nullptr);
                __trackers.emplace_back(new RuneTracker(min_delta_angle_rune));
            }
        }
    }
}

/**
 * @brief 及时删除多帧为空的序列
 */
void RuneDetector::eraseNullTracker()
{
    // 删除
    __trackers.erase(remove_if(__trackers.begin(), __trackers.end(),
                               [&](tracker_ptr &t1)
                               {
                                   return t1->isVanish();
                               }),
                     __trackers.end());
}
