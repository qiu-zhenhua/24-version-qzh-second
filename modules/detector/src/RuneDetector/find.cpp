/**
 * @file find.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief find
 * @version 1.0
 * @date 2021-09-21
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "RuneDetector.h"
#include <opencv2/imgproc.hpp>
#include "RuneParam.h"

using namespace std;
using namespace cv;

/**
 * @brief 找出所有目标
 * 
 * @param src 预处理之后的图像
 * @param features 找到的所有特征
 * @param combos 找到的所有组合体
 * @return vector<combo_ptr> - 存储所有找到的组合体的向量
 */
void RuneDetector::find(Mat &src, vector<feature_ptr> &features, std::vector<combo_ptr> &combos)
{
    // 轮廓二维向量
    vector<vector<Point>> contours;
    // 轮廓等级向量
    vector<Vec4i> hierarchy;
    // 神符轮廓识别
    findContours(src, contours, hierarchy, RETR_TREE, CHAIN_APPROX_NONE);
    // 神符旋转中心向量 临时数据
    vector<RuneA_ptr> rune_armors;
    vector<RuneC_ptr> rune_centers;
    // 遍历循环每一个轮廓
    for (size_t i = 0; i < contours.size(); i++)
    {
        float rune_area = contourArea(contours[i]);
        
        //神符装甲板面积是否符合
        if (rune_area < 80.f)
        {
            continue;
        }
        if (rune_area > 100.f)
        {   
            if(rune_area<3000.f)
            {
                //匹配神符装甲板
                RuneA_ptr temp_rune_armor = make_shared<RuneArmor>(contours[i], hierarchy[i]);
                if (temp_rune_armor->isFeature())
                {
                    rune_armors.push_back(temp_rune_armor);
                    // cout<<"area"<<rune_area<<endl;
                   
                }
            }
            if(rune_area<200.f)
            {
                float rune_length= arcLength(contours[i],true);
                float rune_round_length=2*rune_area/(sqrt(rune_area/3.1415926));
                RuneC_ptr temp_rune_center = make_shared<RuneCenter>(contours[i], hierarchy[i]);
                if (temp_rune_center->isFeature()&&((rune_length/rune_round_length)>1.3)&&(temp_rune_center->getRatio()<1.5))
                {
                    rune_centers.push_back(temp_rune_center);
                    // cout<<temp_rune_center->getCenter()<<endl;
                    // cout<<"area: "<<rune_area<<endl;
                    // cout<<"ratio: "<<temp_rune_center->getRatio()<<endl;
                    // cout<<"length: "<<rune_length<<endl;
                    // cout<<"round_length: "<<rune_round_length<<endl;
                }
            }

        }
        // 匹配神符旋转中心(由于中心面积过小，故移到里面)
        // else
        // {
        //     RuneC_ptr temp_rune_center = make_shared<RuneCenter>(contours[i], hierarchy[i]);
        //     if (temp_rune_center->isFeature())
        //     {
        //         rune_centers.push_back(temp_rune_center);
        //     }
        // }
    }
    // 获取神符
    __current_combos = getRune(rune_armors, rune_centers);
}

/**
 * @brief 获取神符
 * 
 * @param rune_armors 所有神符装甲板
 * @param rune_centers 所有神符旋转中心
 * @return vector<combo_ptr> 
 */
vector<combo_ptr> RuneDetector::getRune(vector<RuneA_ptr> &rune_armors, vector<RuneC_ptr> &rune_centers)
{
    vector<combo_ptr> combos;
    // 神符装甲板、神符旋转中心两两匹配
    for (auto rune_armor : rune_armors)
    {
        for (auto rune_center : rune_centers)
        {
            auto tmp_rune = make_shared<Rune>(rune_armor, rune_center);
            if (tmp_rune->isCombo() &&
                !tmp_rune->isActive())
            {
                combos.emplace_back(tmp_rune);
            }
        }
    }
    return combos;
}
