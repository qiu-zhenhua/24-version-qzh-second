#include "detector.h"

using namespace std;
using namespace cv;

void detector::detect(Mat &initimg,Mat &outputimg,const int64 &record_time)
{
    this->__current_combos.clear();
    this->__current_features.clear();
    pretreat(initimg,outputimg);
    find(outputimg,this->__current_features,this->__current_combos);
     match(this->__current_combos, record_time);   
}

void detector::pretreat(Mat &initimg,Mat &outputimg)
{
    Mat bin = Mat::zeros(Size(initimg.cols, initimg.rows), CV_8UC1);

    // 并行处理二值化
    parallel_for_(Range(0, initimg.rows),
                  [&](const Range &range)
                  {
                      for (int row = range.start; row < range.end; row++)
                      {
                          uchar *data_src = initimg.ptr<uchar>(row);
                          uchar *data_bin = bin.ptr<uchar>(row);
                          for (int col = 0; col < initimg.cols; col++)
                          {
                              if (__color != 0) // 己方非 RED, 即识别 RED
                              {
                                  if (data_src[3 * col + 2] - data_src[3 * col] > 80)
                                  {
                                      data_bin[col] = 255;
                                  }
                              }
                              else // 己方非 BLUE, 即识别 BLUE
                              {
                                  if (data_src[3 * col] - data_src[3 * col + 2] > 80)
                                  {
                                      data_bin[col] = 255;
                                  }
                              }
                          }
                      }
                  });

    outputimg=bin;
}