#pragma once

#include "detector.h"
#include "RuneTracker.h"


class RuneDetector : public detector
{

public:
    RuneDetector(bool color,cv::Point2f gyro_angle,std::vector<tracker_ptr> &trackers) : detector(color,gyro_angle,trackers)
    {};
    
protected:
    virtual void find(cv::Mat &src , std::vector<feature_ptr> &features,std::vector<combo_ptr> &combos) override;
    virtual void match(std::vector<combo_ptr> &, const int64 &) override;

private:
    std::vector<combo_ptr> getRune(std::vector<RuneA_ptr> &,
                                   std::vector<RuneC_ptr> &); // 获取神符
    void matchRunes(std::vector<combo_ptr> &);                // 神符匹配至时间序列
    void eraseNullTracker();                                  // 删除多帧为空的序列
};

using RuneDetector_ptr = std::shared_ptr<RuneDetector>;