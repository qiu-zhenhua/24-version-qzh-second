#pragma once

#include "detector.h"
#include "ArmorTracker.h"


class ArmorDetector : public detector
{

public:
    ArmorDetector(bool color,cv::Point2f gyro_angle,std::vector<tracker_ptr> &trackers) : detector(color,gyro_angle,trackers)
    {};
    
protected:
    virtual void find(cv::Mat &src , std::vector<feature_ptr> &features,std::vector<combo_ptr> &combos) override;
    virtual void match(std::vector<combo_ptr> &, const int64 &) override;

private:
    std::vector<Blob_ptr> findLightBlobs(cv::Mat &bin);
    std::vector<Armor_ptr> findArmors(std::vector<Blob_ptr> &); // 寻找装甲板
    void matchArmors(std::vector<combo_ptr> &);                 // 装甲板匹配至时间序列
    void eraseErrorArmors(std::vector<Armor_ptr> &);            // 根据匹配误差筛选装甲板
    void eraseNullTracker();                                    // 删除多帧为空的序列
};

using ArmorDetector_ptr = std::shared_ptr<ArmorDetector>;