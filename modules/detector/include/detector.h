#pragma once

#include <iostream>
#include <opencv2/opencv.hpp>
#include <string.h>
#include "tracker.h"

class detector
{protected:
protected:
    bool __color;
    cv::Point2f __gyro_location;                 // 每一帧对应的陀螺仪数据
    std::vector<tracker_ptr> &__trackers;        // 所有组合体的时间序列
    std::vector<feature_ptr> __current_features;
    std::vector<combo_ptr> __current_combos;     // 每一帧所有组合体

public:
    detector(bool color,cv::Point2f gyro_angle,std::vector<tracker_ptr> &trackers):__color(color),__gyro_location(gyro_angle),__trackers(trackers)
    {};

    void detect(cv::Mat &initimg,cv::Mat &outputimg,const int64 &record_time);
protected:
    void pretreat(cv::Mat &initimg,cv::Mat &outputimg);
    virtual void find(cv::Mat &src , std::vector<feature_ptr> &features,std::vector<combo_ptr> &combos)=0;
    virtual void match(std::vector<combo_ptr> &, const int64 &)=0;
};

using detector_ptr = std::shared_ptr<detector>;
