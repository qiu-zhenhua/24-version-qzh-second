#pragma once

#include "processor.h"
#include "ArmorDetector.h"
#include "GravityCompensator.h"
#include "ArmorPredictor.h"
class ArmorProcessor : public processor
{
public:
    ArmorProcessor(bool color,const cv::Point2f &gyro_angle,uint8_t shoot_speed):processor(color,gyro_angle,shoot_speed)
    {
        initprocessor();
       
    }
protected:
    inline virtual void create_detector() {
        __detector = std::make_shared<ArmorDetector>(__color,__gyro_angle,__trackers);
    };
    inline virtual void create_compensator(){
        __compensator = std::make_shared<GravityCompensator>();
    };
    inline virtual void create_predictor(){
        __predictor = std::make_shared<ArmorPredictor>();
    };

};

using ArmorProcessor_ptr = std::shared_ptr<ArmorProcessor>;