#pragma once
#include <iostream>
#include <opencv2/opencv.hpp>
#include <string.h>
#include "detector.h"
#include "compensator.h"
#include "predictor.h"
class processor
{
protected:
    bool __color;
    cv::Point2f __gyro_angle;
    uint8_t __shoot_speed;
    cv::Mat __init_img;
    cv::Mat __output_img;
    std::vector<tracker_ptr> __trackers; // 追踪器
    detector_ptr __detector;
    compensate_ptr __compensator;
    predict_ptr __predictor;

public:
    processor(bool color,const cv::Point2f &gyro,uint8_t shoot_speed) : __color(color),__gyro_angle(gyro),__shoot_speed(shoot_speed)
    {
        this->__trackers.clear();
        // 重新指定大小
        this->__trackers.reserve(32);
        //initprocessor();
    };
    void process();

    inline void loadimg(cv::Mat init_img){__init_img=init_img;};
    inline cv::Mat getoutput_img(){return __output_img;};
    inline std::vector<tracker_ptr> get_trackers(){return __trackers;};
protected:
    void initprocessor()
    {
        create_detector();
        create_compensator();
        create_predictor();

    }
    virtual void create_detector()=0;
    virtual void create_compensator()=0;
    virtual void create_predictor()=0;
};

using processor_ptr =std::shared_ptr<processor>;