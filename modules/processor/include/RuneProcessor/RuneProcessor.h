#pragma once

#include "processor.h"
#include "RuneDetector.h"
#include "GravityCompensator.h"
#include "RunePredictor.h"
class RuneProcessor : public processor
{
public:
    RuneProcessor(bool color,const cv::Point2f &gyro_angle,uint8_t shoot_speed):processor(color,gyro_angle,shoot_speed)
    {
        initprocessor();
       
    }
protected:
    inline virtual void create_detector() {
        __detector = std::make_shared<RuneDetector>(__color,__gyro_angle,__trackers);
    };
    inline virtual void create_compensator(){
        __compensator = std::make_shared<GravityCompensator>();
    };
    inline virtual void create_predictor(){
        __predictor = std::make_shared<RunePredictor>();
    };

};

using RuneProcessor_ptr = std::shared_ptr<RuneProcessor>;