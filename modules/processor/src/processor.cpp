#include "processor.h"

using namespace std;
using namespace cv;

void processor::process()
{

    __detector->detect(__init_img,__output_img,getTickCount());
    __compensator->compensate(__trackers,__gyro_angle,__shoot_speed);
    __predictor->predict(__trackers,0U);
}