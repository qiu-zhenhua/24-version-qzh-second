#pragma once

#include "compensator.h"

// 重力补偿类
class GravityCompensator : public compensator
{
public:
    // 重力补偿核心函数
    virtual void compensate(std::vector<tracker_ptr> &, const cv::Point2f &, uint8_t) override;

private:
    float bulletModel(float, float, float); // 计算真实的 y 坐标
    float getPitch(float, float, float);    // Get the gimbal control angle
};
