/**
 * @file compensator.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 抽象补偿类头文件
 * @version 1.0
 * @date 2021-08-24
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include "tracker.h"

// 抽象补偿类
class compensator
{
public:
    /**
     * @brief 补偿核心函数
     * 
     * @param trackers 所有时间序列追踪器
     * @param gyro_angle 陀螺仪角度 (°)
     * @param shoot_speed 子弹射速 (m/s)
     */
    virtual void compensate(std::vector<tracker_ptr> &trackers,
                            const cv::Point2f &gyro_angle,
                            uint8_t shoot_speed) = 0;
};

// 补偿类共享指针
using compensate_ptr = std::shared_ptr<compensator>;
