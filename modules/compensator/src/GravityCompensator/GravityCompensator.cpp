/**
 * @file GravityCompensator.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 重力模型补偿
 * @version 1.0
 * @date 2021-08-28
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "GravityCompensator.h"
#include "CompensateParam.h"

using namespace std;
using namespace cv;

/**
 * @brief 补偿核心函数
 * 
 * @param trackers 所有时间序列追踪器
 * @param gyro_angle 陀螺仪角度 (°)
 * @param shoot_speed 子弹射速 (m/s)
 */
void GravityCompensator::compensate(vector<tracker_ptr> &trackers,
                                    const Point2f &gyro_angle,
                                    uint8_t shoot_speed)
{
    // 循环计算补偿
    for (auto tracker : trackers)
    {
        float dis = tracker->getDistance() / 1000.f;
        float angle = gyro_angle.y - tracker->getFront()->getRelativeAngle().y + compensate_param.PITCH_COMPENSATE;
        // 抛物线补偿模型
        float x_com = compensate_param.YAW_COMPENSATE;
        float y_com = rad2deg(this->getPitch(dis * cosf(deg2rad(angle)),
                                             dis * sinf(deg2rad(angle)),
                                             static_cast<float>(shoot_speed))) -
                      angle;
        // 子弹飞行时间计算
        float flying_time = dis / (static_cast<float>(shoot_speed) * cosf(deg2rad(y_com + angle)));
        // 更新
        tracker->setCompensate({x_com, y_com});
        tracker->setTime(flying_time);
    }
}

/**
 * @brief 计算真实的 y 坐标
 *
 * @param distance 相机到装甲板的水平距离 (m)
 * @param velocity 枪口射速 (m/s)
 * @param angle 枪口与水平方向的夹角 (rad)
 *
 * @return 世界坐标系下真实的 y 坐标
 */
float GravityCompensator::bulletModel(float distance, float velocity, float angle)
{
    // x(m), v(m/s), angle(rad)
    // 忽略空气阻力影响
    float time = distance / (velocity * cosf(angle));
    return velocity * sinf(angle) * time - g * time * time / 2.f;
}

/**
 * @brief 获得补偿角度
 *
 * @param x 目标离相机的水平宽度
 * @param y 目标离相机的铅垂高度
 * @param velocity 枪口射速
 *
 * @return 补偿角度
 */
float GravityCompensator::getPitch(float x, float y, float velocity)
{
    float y_temp, y_actual, dy;
    float angle = 0.f;
    y_temp = y;
    // 使用迭代法求得补偿角度
    for (int i = 0; i < 50; i++)
    {
        angle = (float)atan2(y_temp, x);
        y_actual = bulletModel(x, velocity, angle);
        dy = y - y_actual;
        y_temp = y_temp + dy;
        if (fabs(dy) < 0.001f)
        {
            break;
        }
    }
    return angle;
}