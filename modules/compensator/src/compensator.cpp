/**
 * @file compensator.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 抽象补偿类
 * @version 1.0
 * @date 2021-08-24
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "compensator.h"
