/**
 * @file RuneCenter.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 
 * @version 1.0
 * @date 2021-09-10
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include "feature.h"

// 神符旋转中心类
class RuneCenter : public feature
{
private:
    float __ratio; // 长宽比

public:
    RuneCenter() : __ratio(0.f) {}                     // 默认构造神符旋转中心
    RuneCenter(std::vector<cv::Point> &, cv::Vec4i &); // 轮廓构造神符旋转中心

    // 获取长宽比
    inline float getRatio() { return __ratio; }

protected:
    void initFeature(std::vector<cv::Point> &) override; // 初始化特征
};

// 神符旋转中心共享指针
using RuneC_ptr = std::shared_ptr<RuneCenter>;
