/**
 * @file LightBlob.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 灯条类头文件
 * @version 1.0
 * @date 2021-08-11
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */
#pragma once

#include "feature.h"

// 灯条类
class LightBlob : public feature
{
public:
private:
    cv::Point2f __top;    // 上顶点
    cv::Point2f __bottom; // 下顶点
    bool __is_matched;    // 是否匹配标志位

public:
    LightBlob();
    LightBlob(std::vector<cv::Point> &);

    // 设置匹配信息
    inline void setMatchMessage(bool &&match) { __is_matched = match; }
    // 获取匹配信息
    inline bool getMatchMessage() { return __is_matched; }
    // 获取灯条顶端点
    inline cv::Point2f getTopPoint() { return __top; }
    // 获取灯条底端点
    inline cv::Point2f getBottomPoint() { return __bottom; }

private:
    void initFeature(std::vector<cv::Point> &) override; // 初始化特征
    void getTruePoint(std::vector<cv::Point> &);         // 提取准确的上下顶点
};

using Blob_ptr = std::shared_ptr<LightBlob>;
