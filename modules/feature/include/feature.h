/**
 * @file feature.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 特征类头文件
 * @version 1.0
 * @date 2021-08-10
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */
#pragma once

#include <opencv2/core/types.hpp>
#include <vector>

// 特征类
class feature : protected cv::RotatedRect
{
protected:
    float __width;                      // 宽度
    float __height;                     // 高度
    float __angle;                      // 角度
    bool __is_feature;                  // 是否合适的标志位
    std::vector<cv::Point2f> __corners; // 角点

public:
    feature();
    feature(std::vector<cv::Point> &);
    feature(const feature &);

    // 获取特征的面积
    inline float getArea() { return size.area(); }
    // 获取特征的中心点
    inline cv::Point2f getCenter() { return center; }
    // 获取特征的宽度
    inline float getWidth() { return __width; }
    // 获取特征的高度
    inline float getHeight() { return __height; }
    // 获取特征的角度
    inline float getAngle() { return __angle; }
    // 获取特征的角点
    inline std::vector<cv::Point2f> getCorners() { return __corners; }
    // 是否为合适特征
    inline bool isFeature() { return __is_feature; }

protected:
    // 初始化特征
    virtual void initFeature(std::vector<cv::Point> &) = 0;
};

using feature_ptr = std::shared_ptr<feature>;
