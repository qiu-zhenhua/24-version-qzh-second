/**
 * @file RuneArmor.h
 * @author 赵曦 (535394140@qq.com)
 * @brief 神符装甲板头文件
 * @version 1.0
 * @date 2021-09-10
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#pragma once

#include "feature.h"

// 神符装甲板类
class RuneArmor : public feature
{
private:
    float __ratio;    // 长宽比
    bool __is_active; // 是否处于激活状态

public:
    RuneArmor() : __ratio(0.f) {}                     // 默认构造神符装甲板
    RuneArmor(std::vector<cv::Point> &, cv::Vec4i &); // 轮廓构造神符装甲板

    // 获取长宽比
    inline float getRatio() { return __ratio; }
    // 是否激活标志位
    inline bool isActive() { return __is_active; }

protected:
    void initFeature(std::vector<cv::Point> &) override; // 初始化特征
};

// 神符装甲板共享指针
using RuneA_ptr = std::shared_ptr<RuneArmor>;
