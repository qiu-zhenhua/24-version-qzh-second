/**
 * @file RuneCenter.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 
 * @version 1.0
 * @date 2021-09-10
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "RuneCenter.h"

using namespace std;
using namespace cv;

/**
 * @brief 轮廓构造神符旋转中心
 * 
 * @param contour 轮廓向量
 * @param hierarchy 等级向量
 */
RuneCenter::RuneCenter(vector<Point> &contour, Vec4i &hierarchy) : feature(contour)
{
    // init
    this->initFeature(contour);
    // match
    if (this->size.area() > 2000.f)
    {
        return;
    }
    // 获取长宽比信息
    __width = max(this->size.width, this->size.height);
    __height = min(this->size.width, this->size.height);
    __ratio = __width / __height;
    // 判断长宽比例是否符合
    if (__ratio > 3.f)
    {
        return;
    }
    // 位于最外层，且无子轮廓，存在若干个并列的轮廓的情况下视为神符旋转中心
    if ((hierarchy[0] != -1 ||
         hierarchy[1] != -1) && // 存在若干并列轮廓
        //hierarchy[2] == -1 &&   // 无子轮廓
        hierarchy[3] == -1)     // 无父轮廓
    {
        this->__is_feature = true;
    }
    // 更新角点信息
    Point2f sortedPoints[4];
    this->points(sortedPoints);
    // 预留4个角点的空间
    __corners.reserve(4);
    for (auto point : sortedPoints)
    {
        __corners.push_back(point);
    }
}

/**
 * @brief 初始化特征信息
 * 
 * @param contour 
 */
void RuneCenter::initFeature(vector<Point> &contour)
{
    __ratio = 0.f;
    __width = 0.f;
    __height = 0.f;
    __is_feature = false;
}
