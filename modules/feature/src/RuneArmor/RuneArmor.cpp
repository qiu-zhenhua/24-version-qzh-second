/**
 * @file RuneArmor.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 神符装甲板
 * @version 1.0
 * @date 2021-09-10
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "RuneArmor.h"
#include "RuneParam.h"

using namespace std;
using namespace cv;

/**
 * @brief 轮廓构造神符装甲板
 * 
 * @param contour 轮廓向量
 * @param hierarchy 等级向量
 */
RuneArmor::RuneArmor(vector<Point> &contour, Vec4i &hierarchy) : feature(contour)
{
    // init
    this->initFeature(contour);
    // 获取长宽比信息
    __width = max(this->size.height, this->size.width);
    __height = min(this->size.height, this->size.width);
    __ratio = __width / __height;
    //判断长宽比例是否符合
    if (__ratio > 1.5f || __ratio < 0.5f)
    {
        return;
    }
    // 位于父轮廓内，仅存在一个轮廓的情况下视为未激活的神符装甲板
    if ((hierarchy[0] != -1 ||
        hierarchy[1] != -1) && // 有并列轮廓，即在父轮廓范围内单独存在
        hierarchy[2] != -1 && // 有子轮廓
        hierarchy[3] != -1)   // 存在父轮廓
    {
        __is_feature = true;
        __is_active = false;
    }
    // 位于父轮廓内，存在多个轮廓的情况下视为已激活的神符装甲板
    else if ((hierarchy[0] != -1 ||
              hierarchy[1] != -1) && // 存在并列关系轮廓，即在父轮廓范围内非单独存在
             hierarchy[2] != -1 &&   // 有子轮廓
             hierarchy[3] == -1)     // 无父轮廓
    {
        __is_feature = true;
        __is_active = true;
    }
    // 更新角点信息
    Point2f sortedPoints[4];
    this->points(sortedPoints);
    // 预留4个角点的空间
    __corners.reserve(4);
    for (auto point : sortedPoints)
    {
        __corners.push_back(point);
    }
}

/**
 * @brief 初始化特征信息
 * 
 * @param contour 
 */
void RuneArmor::initFeature(vector<Point> &contour)
{
    __ratio = 0.f;
    __width = 0.f;
    __height = 0.f;
    __is_active = true;
    __is_feature = false;
}
