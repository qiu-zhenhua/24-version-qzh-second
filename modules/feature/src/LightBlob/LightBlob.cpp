/**
 * @file LightBlob.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 
 * @version 1.0
 * @date 2021-08-11
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include "Math.h"
#include "LightBlobParam.h"
#include "LightBlob.h"

using namespace cv;
using namespace std;

/**
 * @brief 默认构造灯条
 */
LightBlob::LightBlob()
{
    __top = Point2f(0.f, 0.f);
    __bottom = Point2f(0.f, 0.f);
    __is_matched = false;
}

/**
 * @brief 使用轮廓构造灯条
 * 
 * @param contour 轮廓
 */
LightBlob::LightBlob(vector<Point> &contour) : feature(contour)
{
    __top = Point2f(0.f, 0.f);
    __bottom = Point2f(0.f, 0.f);
    __is_matched = false;
    this->initFeature(contour);
}

/**
 * @brief 初始化特征信息
 * 
 * @param contour 
 */
void LightBlob::initFeature(vector<Point> &contour)
{
    // init
    if (contour.empty())
    {
        return;
    }
    // 统一角度方向
    this->__angle = (this->angle > 90 ? this->angle - 180 : this->angle);
    // 获取灯条长宽比例
    float lw_ratio = this->size.height / this->size.width;
    // 长度过小，排除
    if (this->size.height < light_blob_param.MIN_LENGTH)
    {
        return;
    }
    // 判断为远处灯条
    else if (this->size.height <= light_blob_param.CLOSE_LENGTH)
    {
        // 远处灯条不符合比例
        if (lw_ratio > light_blob_param.MAX_FAR_RATIO ||
            lw_ratio < light_blob_param.MIN_FAR_RATIO)
        {
            return;
        }
    }
    // 判断为近处灯条
    else
    {
        if (lw_ratio > light_blob_param.MAX_CLOSE_RATIO) // 近处灯条不符合比例
        {
            return;
        }
    }

    // 长宽比例过大
    if (lw_ratio > light_blob_param.MAX_RATIO)
    {
        return;
    }

    Point2f sortedPoints[4];
    this->points(sortedPoints);
    // 预留4个角点的空间
    __corners.reserve(4);
    for (auto point : sortedPoints)
    {
        __corners.push_back(point);
    }
    // 点从上到下排序
    sort(__corners.begin(), __corners.end(),
         [&](Point2f &pt_1, Point2f &pt_2) -> bool
         {
             return pt_1.y < pt_2.y;
         });
    // 获得上顶点
    this->__top = (__corners[0] + __corners[1]) / 2;
    // 获得下顶点
    this->__bottom = (__corners[2] + __corners[3]) / 2;
    // 根据斜率排除误识别
    if (fabs(this->__angle) > light_blob_param.MAX_ANGLE)
    {
        return;
    }
    // 修正灯条匹配误差
    this->getTruePoint(contour);

    this->__height = this->size.height;
    this->__width = this->size.width;
    // 更新标志位
    this->__is_feature = true;
}
/**
 * @brief 提取准确的上下顶点
 * 
 * @param contour 
 */
void LightBlob::getTruePoint(vector<Point> &contour)
{
    float min_y = this->center.y;
    float max_y = this->center.y;
    Point bottom = Point(-1, -1);
    Point top = Point(-1, -1);
    float min_distance_top = MAXFLOAT;
    float min_distance_botoom = MAXFLOAT;
    // 遍历每一个轮廓点
    for (auto point : contour)
    {
        // 高于或低于一定高度时才对其进行计算比较，节省运算
        if (point.y < min_y)
        {
            // 找出离原来上顶点最近的点作新上顶点
            float tmp_distance = getDistances(point, this->__top);
            if (tmp_distance < min_distance_top)
            {
                min_distance_top = tmp_distance;
                top = point;
            }
        }
        if (point.y > max_y)
        {
            // 找出离原来下顶点最近的点作新下顶点
            float tmp_distance = getDistances(point, this->__bottom);
            if (tmp_distance < min_distance_botoom)
            {
                min_distance_botoom = tmp_distance;
                bottom = point;
            }
        }
    }
    // 提取轮廓点集中离拟合椭圆上下顶点最近的点作修正后的数据
    if (top != Point(-1, -1) &&
        bottom != Point(-1, -1))
    {
        // 更新上下顶点
        this->__top = top;
        this->__bottom = bottom;
        // 更新灯条中点
        this->center = (this->__top + this->__bottom) / 2.f;
        float temp_height = getDistances(this->__top, this->__bottom);
        this->size.width = this->size.width * temp_height / this->size.height;
        this->size.height = temp_height;
    }
}