/**
 * @file feature.cpp
 * @author 赵曦 (535394140@qq.com)
 * @brief 特征类
 * @version 1.0
 * @date 2021-08-10
 * 
 * @copyright Copyright SCUT RobotLab(c) 2021
 * 
 */

#include <opencv2/imgproc.hpp>
#include "feature.h"

using namespace std;
using namespace cv;

/**
 * @brief 默认构造特征
 * 
 * @param contour
 */
feature::feature()
{
    // init
    __is_feature = false;
    __width = 0.f;
    __height = 0.f;
    __angle = 0.f;
}

/**
 * @brief 利用轮廓构造特征
 * 
 * @param contour
 */
feature::feature(std::vector<cv::Point> &contour) : RotatedRect(fitEllipse(contour))
{
    // init
    __is_feature = false;
    __width = 0.f;
    __height = 0.f;
    __angle = 0.f;
}

/**
 * @brief 复制构造特征
 * 
 * @param __feature 
 */
feature::feature(const feature& __feature)
{
    __is_feature = __feature.__is_feature;
    __width = __feature.__width;
    __height = __feature.__height;
    center = __feature.center;
}
